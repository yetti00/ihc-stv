var widgetAPI = new Common.API.Widget();
var tvKey = new Common.API.TVKeyValue();
var apiAddr = "http://" + apiServer + "/api";
var scenesAddr = apiAddr + "/scenes";
var addr = "http://" + apiServer + "/api/rooms";
var dev_addr = "http://" + apiServer + "/api/devices/";
var floorplans = null;
var floorplanImgs = new Object();
var current_section = "rooms";
var prev_section = "rooms";
var first_room = true;
var first_device = true;
var first_fav = true;
var devices = new Array();
var max_width = 610;
var max_height = 520;
var wsUrl = null;
var websocket = null;
var current_device;
var strFavIds = getFile("favIds");
var favouriteIds = JSON.parse(strFavIds);
function setWebsocket(url){
	wsUrl = url;
	if(typeof window.WebSocket !== 'undefined'){
		websocket = new WebSocket(wsUrl);
		websocket.onmessage = function(event){
			updateInfo(event.data);
			alert(event.data);
		}
		websocket.onclose = function(data){
			setTimeout("websocket = new WebSocket(wsUrl);", 5000);
		}
	}else{
		alert("WebSocket not defined");
	}
}

function renderTranslations(){
	$("#rooms_label").text(LNG_ROOM);
	$("#devices_label").text(LNG_DEVICES);
	$("#favorites_label").text(LNG_FAVOURITES);
	$("#scenes_label").text(LNG_SCENES);
	
	$("#rooms_header").text(LNG_LOADING_DATA);
	$("#devices_header").text(LNG_LOADING_DATA);
	$("#favorites_header").text(LNG_LOADING_DATA);
	$("#scenes_header").text(LNG_LOADING_DATA);
};

var Main =
{

};
Main.onLoad = function()
{
	 // Enable key event processing
    this.enableKeys();
    widgetAPI.sendReadyEvent();
    renderTranslations();
    $("#bottombar").sfKeyHelp({
            'play':LNG_SETTINGS,
            'stop':LNG_CAMERAS,
    });
    
    
    $.getJSON(apiAddr, function(data) {
    		if(!data)
    			return;
    	
            if(typeof data.notifications != 'undefined'){
                    setWebsocket(data.notifications);
                    setFloorplans(data.floorplans);
            }
    });
    $.getJSON(dev_addr, function(data) {
            var first_device = true;
            if(!data){
            	 $("#devices_header").text(LNG_DATA_FAILED);
            	 $("#favorites_header").text(LNG_DATA_FAILED);
            	 return;
            }
            
            $.each(data, function(key, val) {
                    $.getJSON(val.url, function(data_device) {
                            var booleanState = getBooleanState(data_device);
                            var on = (booleanState ?  '  on="on" ' : '');
                            var favsNum = 0;
                            if(!$("#favorites > ul").find("#fav_" + key).length && isDeviceFavourite(key)){
                                    $("#favorites > ul").append('<li dev_type="' + data_device['device info'].type + '" class="devId ' + (first_fav ? ' selected ' : '') +'" id="fav_' + key + '" ' + on + ' ><div class="device_label">' + data_device['device info'].label + '</div><div dev_type="' + data_device['device info'].type + '" class="device_image"></div><div ' + on + 'class="device_check"></div></li>');
                                    first_fav = false;
                                    favsNum++;
                            }
                            if(favsNum == 0){
                                    $("#favorites_header").text(LNG_NO_FAVOURITES);
                            }
                            if(!$("#info_"+key).length){
                                    createDeviceInfo(data_device, first_device);
                                    createDeviceActions(data_device);
                            }
                            first_device = false;
                    });
            });
            
            if(first_device){
                $("#devices_header").text(LNG_NO_DEVICES);
                $("#favorites_header").text(LNG_NO_FAVOURITES);
        } 
    });
    $.getJSON(addr, function(data) {
    	if(!data){
       	 	$("#rooms_header").text(LNG_DATA_FAILED);
       	 	return;
    	}
            $.each(data, function(key, val) {
                    
                    $.getJSON(val.url, function(data_room) {
                            $("#devices").append('<div '+ (first_room ? '': 'style="display:none" ') + ' room="' + key + '"" id="devices_' + key + '" class="devices_room" ><ul></ul></div>');
                            $("#rooms > ul").append('<li id="' + key + '" '+ (first_room ? 'class="selected" ': '') + '><div class="room_label">' + data_room['room info'].label + '</div><div dev_type="room" class="device_image"></div></li>');
                            if(first_room){
                                    $("#plan_name").text(data_room['room info'].label);
                                    $("#rooms_header").text(data_room['room info'].label);
                            }
                            $("#plan").append('<div '+ (first_room ? '': 'style="display:none" ') + 'class="plan" id="plan_' + key + '"></div>');
                            if(data_room.floorplan!=null){
                            		drawFloorplan(key, data_room.floorplan.image);
                                    
                            }
                            $.each(data_room.devices, function(key2, val2) {
                                    devices.push(key2);
                                    $.getJSON(dev_addr + key2, function(data_device) {
                                            var booleanState = getBooleanState(data_device);
                                            var on = (booleanState ?  '  on="on" ' : '');
                                            $("#devices_" + key + " > ul").append('<li dev_type="' + data_device['device info'].type + '" class="devId ' + (first_device ? ' selected ' : '') +'" id="' + key2 + '" ' + on + ' ><div class="device_label">' + data_device['device info'].label + '</div><div dev_type="' + data_device['device info'].type + '" class="device_image"></div><div ' + on + 'class="device_check"></div></li>');
                                            
                                            if(val2.coordinates!=null){
                                                    
                                                    $('#plan_' + key).append("<li class='status' id='status_" + key2 + "' " + on + " dev_type='" + data_device['device info'].type + "'><div style='margin:0;' class='device_image' dev_type='" + data_device['device info'].type + "'></div></li>");
                                                    var left = Math.floor(val2.coordinates[0] * max_width);
                                                    var top = Math.floor(val2.coordinates[1] * max_height);
                                                    if(left >= (max_width-10)){
                                                            left = max_width-11;
                                                    }
                                                    if(left < 3){
                                                            left += 3;  
                                                    }
                                                    if(top >= (max_height-10)){
                                                            top = max_height-11;
                                                    }
                                                    if(top < 3){
                                                            top += 3;
                                                    }
                                                    var status = $('#plan_' + key).find("#status_" + key2);
                                                    
                                                    status.css('left', left + 'px');
                                                    status.css('top', top + 'px');
                                                    
                                                    
                                            }
                                            
                                            
                                            setDeviceInfo(val2.url);
                                            if(first_device){
                                                    $("#devices_header").text( data_device['device info'].label);
                                                    current_device = key2;
                                            }
                                            first_device = false;
                                    });
                            });
                            
                            first_room  = false;
                    });
            });
         if(first_room){
                $("#rooms_header").text(LNG_NO_ROOMS);
        } 
    });
    
    loadScenes();
	
};

Main.onUnload = function()
{

};

Main.enableKeys = function()
{
	document.getElementById("anchor").focus();
};

Main.keyDown = function(event)
{
	var keyCode = event.keyCode;
	alert("Key pressed: " + keyCode);

	switch(keyCode)
	{
		case tvKey.KEY_EXIT:
			$.sf.exit();
			widgetAPI.sendExitEvent();
			return true;
		break;
		case tvKey.KEY_RETURN:
		case tvKey.KEY_PANEL_RETURN:
			alert("RETURN");
			//widgetAPI.sendReturnEvent();
			previousSection();
			event.preventDefault();
			return false;
			break;
		case tvKey.KEY_LEFT:
			alert("LEFT");
			if(current_section == "rooms"){
				prevRoom();
				return;
			}
			if(current_section == "devices"){
				prevDevice();
				return;
			}
			if(current_section == "favorites"){
				prevFavDevice();
				return;
			}
			if(current_section == "scenes"){
				prevScene();
				return;
			}
			break;
		case tvKey.KEY_RIGHT:
			alert("RIGHT");
			if(current_section == "rooms"){
				nextRoom();
				return;
			}
			if(current_section == "devices"){
				nextDevice();
				return;
			}
			if(current_section == "favorites"){
				nextFavDevice();
				return;
			}
			if(current_section == "scenes"){
				nextScene();
				return;
			}
			break;
		case tvKey.KEY_UP:
			alert("UP");
			if(current_section == "rooms"){
				prevRoom(6);
				return;
			}
			if(current_section == "devices"){
				prevDevice(6);
				return;
			}
			if(current_section == "favorites"){
				prevFavDevice(6);
				return;
			}
			if(current_section == "scenes"){
				prevScene(6);
				return;
			}
			break;
		case tvKey.KEY_DOWN:
			alert("DOWN");
			if(current_section == "rooms"){
				nextRoom(6);
				return;
			}
			if(current_section == "devices"){
				nextDevice(6);
				return;
			}
			if(current_section == "favorites"){
				nextFavDevice(6);
				return;
			}
			if(current_section == "scenes"){
				nextScene(6);
				return;
			}
			break;
		case tvKey.KEY_ENTER:
		case tvKey.KEY_PANEL_ENTER:
			alert("ENTER");
			if(current_section=="actions"){
				var current = $(".selected");
				if(current.length){
					switch_device(current);
				}
				return false;
			}
			if (current_section == "devices") {
				var id = $("#devices .selected").attr("id");
				var type = $("#devices .selected").attr("dev_type");
				alert("dev change");
				var current = $("#info_" + id + " .action_button");
				if (current.length) {
					switch_device(current);
				}
				var current = $("#info_"+id+" .up_down_button");
				if(current.length){
					switch_device(current);
				}
				var current = $("#left_info_"+id+" .brightness");
				if(current.length){
					if (type == "rgb light"){
						switch_rgb_brightness(current, $("#left_info_" + id).attr(
							"last_num"), id);
					}else{
						switch_brightness(current);
					}
				}
				return false;
		}
			if(current_section=="favorites"){
				var id= $("#favorites .selected").attr("id").replace("fav_", "");
				var type = $("#favorites .selected").attr("dev_type");
				alert("fav change");
				var current = $("#info_"+id+" .action_button");
				if(current.length){
					switch_device(current);
				}
				var current = $("#info_"+id+" .up_down_button");
				if(current.length){
					switch_device(current);
				}
				var current = $("#left_info_"+id+" .brightness");
				if(current.length){
					if (type == "rgb light"){
						switch_rgb_brightness(current, $("#left_info_" + id).attr(
							"last_num"), id);
					}else{
						switch_brightness(current);
					}
				}
				return false;
			}
			if(current_section=="rooms"){
				showDevices();
				
			}
			if(current_section == "scenes"){
				sendScene();
			}
			break;
		case tvKey.KEY_PLAY:
			alert("PLAY");
			window.location.href= "setting.html" + window.location.search;
			break;
		case tvKey.KEY_STOP:
			window.location.href= "camera.html" + window.location.search;
			break;
		case tvKey.KEY_RED:
			alert("RED");
			showRooms();
			break;
		case tvKey.KEY_GREEN:
			alert("GREEN");
			showDevices();
			break;
		case tvKey.KEY_YELLOW:
			alert("YELLOW");
			showFavs();
			break;
		case tvKey.KEY_BLUE:
			alert("YELLOW");
			showScenes();
			break;
		case tvKey.KEY_FF:
		case tvKey.KEY_VOL_UP:
		case tvKey.KEY_PANEL_VOL_UP:
			alert("FF");
			if(current_section=="devices"){
				var id= $("#devices .selected").attr("id");
				var type = $("#devices .selected").attr("dev_type");
				alert("brightness_up");
				var current = $("#left_info_"+id+" .brightness");
				if(current.length){
					if (type == "rgb light"){
						brightness_up_rgb(current, $("#left_info_" + id).attr(
							"last_num"), id);
					}else{
						brightness_up(current);
					}
				}
				return;
			}
			if(current_section=="favorites"){
				var id= $("#favorites .selected").attr("id").replace("fav_", "");
				alert("fav change");
				var type = $("#favorites .selected").attr("dev_type");
				var current = $("#left_info_"+id+" .brightness");
				if(current.length){
					if (type == "rgb light"){
						brightness_up_rgb(current, $("#left_info_" + id).attr(
							"last_num"), id);
					}else{
						brightness_up(current);
					}
				}
				return false;
			}
			break;
		case tvKey.KEY_RW:
		case tvKey.KEY_VOL_DOWN:
		case tvKey.KEY_PANEL_VOL_DOWN:
			alert("RW");
			if(current_section=="devices"){
				var id= $("#devices .selected").attr("id");
				var type = $("#devices .selected").attr("dev_type");
				alert("brightness_down");
				var current = $("#left_info_"+id+" .brightness");
				if(current.length){
					if (type == "rgb light"){
						brightness_down_rgb(current, $("#left_info_" + id).attr(
							"last_num"), id);
					}else{
						brightness_down(current);
					}
				}
				return;
			}
			if(current_section=="favorites"){
				var id= $("#favorites .selected").attr("id").replace("fav_", "");
				alert("fav change");
				var type = $("#favorites .selected").attr("dev_type");
				var current = $("#left_info_"+id+" .brightness");
				if(current.length){
					if (type == "rgb light"){
						brightness_down_rgb(current, $("#left_info_" + id).attr(
							"last_num"), id);
					}else{
						brightness_down(current);
					}
				}
				return false;
			}
			break;
		case tvKey.KEY_1:
			numPressed(1);
			break;
		case tvKey.KEY_2:
			numPressed(2);
			break;
		case tvKey.KEY_3:
			numPressed(3);
			break;
		case tvKey.KEY_4:
			numPressed(4);
			break;
		case tvKey.KEY_5:
			numPressed(5);
			break;
		case tvKey.KEY_6:
			numPressed(6);
			break;
		case tvKey.KEY_7:
			numPressed(7);
			break;
		case tvKey.KEY_8:
			numPressed(8);
			break;
		case tvKey.KEY_9:
			numPressed(9);
			break;
		case tvKey.KEY_0:
			numPressed(0);
			break;
		default:
			alert("Unhandled key");
			break;
	}
};

function setDeviceInfo(url){
	
}
function showRoom(id){
	$("#devices").children("div:not(#devices_header)").hide();
	$("#devices").find(".selected").removeClass("selected");
	var $roomDevices = $("#devices").children("[id='devices_"+ id +"']");
	$roomDevices.show();
	$roomDevices.find("li").first().addClass("selected");
	showDevice($roomDevices.find("li").first().attr("id"));
	$(".plan").hide();
	$(".plan[id='plan_"+ id +"']").show();
	var $room = $("#rooms").find(".selected");
	$("#plan_name").text($room.find(".room_label").text());
	$("#rooms_header").text($room.find(".room_label").text());
}

function createDeviceInfo(data, first_device){
	var booleanState = getBooleanState(data);
	$("#device_info").append("<div class='device_info' " + (first_device ? " style='display:block;' ": "") +"id='info_" + data.id + "'  dev_type='" + data['device info'].type + "' " + (booleanState ?  ' on="on" ' : '') + " ></div>");
	 
	var html = "";
	html += "<div class='device_image'></div>";
	html += "<div class='info_info'>";
	html += LNG_NAME + ": "+data['device info'].label+"<br>";
	html += LNG_TYPE + ": "+data['device info'].type+"<br>";
	html += LNG_PRODUCT + ": "+data['device info']['product type']+"<br>";
	html += "</div>";
	
	html += "<div class='info_state'>";
	html += updateInfoState(data);
	html += "</div>";
	$("#info_" + data.id).append(html);
}
function updateInfoState(data){
	html = '';
	if(hasBooleanState(data)){
		html = "<span class='device_state_txt'>" + LNG_DEVICE_STATE + ": </span><span class='device_state_txt_on_off'>" + getTextState(data) + "</span>";
	}
	return html;
}
function createDeviceActions(data){
	//$("#device_info").append("<div class='device_action' id='action_" + data.id + "'></div>");
	 
	var html = "";

	html += "<div class='info_actions'>";
	html += "<ul class='actions'>"
	for(var key in data['actions info']){
		var val = data['actions info'][key];
		if(key=='on' && val.type=='bool' && typeof data.state!='undefined'){
			var to = data.state[key] ? 'off' : 'on';
			var to_bool = data.state[key] ? 'false' : 'true';
			html += "<li class='action_button' action_addr='"+ (dev_addr + data.id) +"' action_name='"+key+"' action_param='"+to_bool+"' ></li>"
		}
		
	}
	if(typeof data.state!='undefined' && typeof data['state']['roll up'] != 'undefined'){
		var to_bool = data.state['roll up'] ? 'roll down' : 'roll up';
		html += "<div class='up_down_button' action_addr='"+ (dev_addr + data.id) +"' action_name='roll up' action_param='"+to_bool+"' ></div>"
	}
	html += "</ul></div>";
	$("#info_" + data.id).append(html);
	if(typeof data.state!='undefined' && typeof data['actions info']!='undefined' && typeof data['actions info'].brightness != 'undefined'){
		$("#left_device_info").append("<div class='left_device_info' " + (first_device ? " style='display:block;' ": "") +" id='left_info_" + data.id + "'  dev_type='" + data['device info'].type + "' ><div class='left_info_top'></div><div class='left_info_bottom'></div></div>");
		
		var brightnessPercent = 100 * (data.state.brightness/(data['actions info'].brightness.max - data['actions info'].brightness.min));
		if(typeof data['actions info'].blue != 'undefined'){
			html = "<div class='color color1'></div><div class='color color2'></div><div class='color color3'></div><div class='color color4'></div><div class='color color5'></div>" +
					"<div class='color color6'></div><div class='color color7'></div><div class='color color8'></div><div class='color color9'></div>";
			$("#left_info_" + data.id + " > .left_info_top").append(html);
			var colorPos = getColorPos(data);
			$("#left_info_" + data.id).find(".color").removeAttr("active_color");
			$("#left_info_" + data.id).find(".color" + (colorPos + 1)).attr("active_color", "active_color");
			$("#left_info_" + data.id).attr("last_num", (colorPos + 1));
		} else{
			html ="<div class='brightness_top_info'>" + LNG_BRIGHTNESS + "</div>";
			$("#left_info_" + data.id + " > .left_info_top").append(html);
		}
		
		html = "<br /><div class='brightness_container_before'></div><div class='brightness_container'>" +
				"<div class='brightness' max='" + data['actions info'].brightness.max  + "' min='" + data['actions info'].brightness.min + "' url='"+ (dev_addr + data.id) +"' action_name='brightness' " +
						"style='width:" + brightnessPercent +"%;' step='" + data['actions info'].brightness.step  + "' value='" + data.state.brightness + "'><img src='images/540/slider_jas-10.png' /></div></div><div class='brightness_container_after'></div><div class='brightness_container_help'>VOL UP/DOWN</div>";
		$("#left_info_" + data.id + " > .left_info_bottom").append(html);

	}
	
}
function showDevice(id, from){
	$(".status").css("border-color", "black");
	$("#status_"+ id).css("border-color", "orange");
	$(".device_info").hide();
	$("#info_"+ id).show();
	$(".left_device_info").hide();
	$("#left_info_"+ id).show();
	var $device = $("#" + from).find(".selected");
	$("#" + from + "_header").text($device.find(".device_label").text());
	current_device = id;
	
}
function showScene(id){
	var $scene = $("#scenes").find(".selected");
	$("#scenes_header").text($scene.find(".device_label").text());
}
function getStatusColor(data){
	if(data['device info'].type=='light'|| data['device info'].type=='heating' || data['device info'].type=='blinds'){
		if(data.state.on==true || data.state.up==true)
			return 'green';
		else 
			return 'red';
	}else{
		return 'yellow';
	}
}
function switch_device(el){
	var act = el.attr('action_name');
	var state = el.attr('action_param');
	if(act == "roll up"){
		act = state;
		state = null;
	}else{
		var state = state=='true'?true:false;
	}
	var url =  el.attr('action_addr');
	var update_func = updateStatus;
	send_simple_action(url, act, state, update_func, el);
	
	 
}
function switch_brightness($el){
	var value = parseInt($el.attr("value"));
	var lastValue = parseInt($el.attr("last_value"));
	var max =  parseInt($el.attr("max"));
	var min =  parseInt($el.attr("min"));
	if(value === min){
		var oldValue = value;
		if(!isNaN(lastValue) && lastValue!=min){
			value = lastValue;
			lastValue = oldValue;
		}else{
			value = max;
			lastValue = min;
		}
	}else{
		lastValue = value;
		value = min;
	}
	$el.attr('last_value', lastValue);
	var url = $el.attr("url");
	var action_name = $el.attr("action_name");
	
	send_simple_action(url, action_name, value, null, $el);
}
function switch_rgb_brightness($el, num, id){
	var value = parseInt($el.attr("value"));
	var lastValue = parseInt($el.attr("last_value"));
	var max =  parseInt($el.attr("max"));
	var min =  parseInt($el.attr("min"));
	if(value === min){
		var oldValue = value;
		if(!isNaN(lastValue) && lastValue!=min){
			value = lastValue;
			lastValue = oldValue;
		}else{
			value = max;
			lastValue = min;
		}
	}else{
		lastValue = value;
		value = min;
	}
	$el.attr('last_value', lastValue);
	alert("setting:"+num);
	var data_obj = new Object();
	data_obj['red'] = colors[num - 1][0];
	data_obj['green'] = colors[num - 1][1];
	data_obj['blue'] = colors[num - 1][2];
	data_obj['brightness'] = value;
	data_json = JSON.stringify(data_obj);
	alert(data_json);
	$.ajax({
		url : dev_addr + id,
		type : "PUT",
		dataType : "json",
		contentType : "json",
		data : data_json,
		success : function(data) {
			alert(data);
		},
		error : function(data) {
			alert("Error performing device update");
		}
	});
}
function send_simple_action(url, name, value, update_func, el){
	var data_obj = new Object();
	data_obj[name] = value;
	data_json = JSON.stringify(data_obj);
	alert(url);
	alert(data_json);
	$.ajax({  
		  url:url,  
		  type: "PUT",  
		  dataType: "json",  
		  contentType: "json",  
		  data: data_json,  
		  success: function(data){  
			  alert(data);
		    if(data!= null && data.status=='OK'){
		    	if(update_func){
		    		update_func(el);
		    	}
		    }
		  },  
		  error: function(data){  
		    alert("Error performing device update");  
		  }  
		});
}
function updateStatus(el){
	if(el.attr('action_param') == 'true'){
		el.attr('action_param', 'false')
	}else if(el.attr('action_param') == 'false'){
		el.attr('action_param', 'true')
	}
	updateInfo(el.attr('action_addr'));
}
function updateInfo(url){
	$.getJSON(url, function(data_device) {
		$("#info_" + data_device.id).find(".info_state").html(updateInfoState(data_device));
		var status = $("#status_" + data_device.id);
		
		var devInfo = $("#info_" + data_device.id);
		var actionButton = $("#info_" + data_device.id+" .action_button");
		if(getBooleanState(data_device)){
			status.attr("on", "on");
			devInfo.attr("on", "on");
			
			if(typeof data_device['state']['roll up']!='undefined'){
				$("#info_" + data_device.id+" .up_down_button").attr("action_param", "roll down");
			}else{
				actionButton.attr("action_param", "false");
			}
		}else{
			status.removeAttr("on");
			devInfo.removeAttr("on");
			if(typeof data_device['state']['roll up']!='undefined'){
				$("#info_" + data_device.id+" .up_down_button").attr("action_param", "roll up");
			}else{
				actionButton.attr("action_param", "true");
			}
		}
		$(".devices_room").each(function(){
			var dev = $(this).find("#" + data_device.id);
			var devCheck = $(this).find("#" + data_device.id + " .device_check");
			if(dev.length){
				if(dev.length && getBooleanState(data_device)){
					dev.attr("on", "on");
					devCheck.attr("on", "on");
				}else{
					dev.removeAttr("on");
					devCheck.removeAttr("on");
				}
			}
		});
		var $fav = $("#fav_" + data_device.id);
		
		if($fav.length){
			var $favCheck = $fav.find(".device_check");
			if(getBooleanState(data_device)){
				$fav.attr("on", "on");
				$favCheck.attr("on", "on");
			}else{
				$fav.removeAttr("on");
				$favCheck.removeAttr("on");
			}
		}
		
		if(typeof data_device.state.brightness !='undefined'){
				update_brightness(data_device);
		}
	});
}
function updateAllInfo(){
	$(".devId").each(function(){
		var id = $(this).attr('id');
		updateInfo(dev_addr+id);
	});
}
function getBooleanState(data){
	if(data.state != null){
		if(data.state.on==true || data.state.up==true || data.state.brightness > 0 || data.state['roll up']==true){
			return true;
		}else{
			return false;
		}
	}
}
function getTextState(data){
	if(data.state != null){
		if(data.state.on==true || data.state.up==true || data.state.brightness > 0){
			return "ON";
		}else if(data.state['roll up'] == true){
			return "UP";
		}else if(data.state['roll up'] == false){
			return "DOWN";
		}else{
			return "OFF";
		}
	}
}
function hasBooleanState(data){
	if(data.state != null){
		if(typeof data.state.on!='undefined' || typeof data.state.up!='undefined' || typeof data.state.brighntness!='undefined' || typeof data.state['roll up']!='undefined'){
			return true;
		}else{
			return false;
		}
	}
}
function brightness_up($el){
	var value = parseInt($el.attr("value"));
	var step =  parseInt($el.attr("step"));
	var max =  parseInt($el.attr("max"));
	var min =  parseInt($el.attr("min"));
	
	if(step == 1){
		var range = max - min;
		step =  Math.ceil(range/10);
		var count = value/step;
		value = count * step + min;
		alert(step);
	} 
	
	if(value<=(max - step)){
		value += step;
	}else{
		value = max;
	}
	var url = $el.attr("url");
	var action_name = $el.attr("action_name");
	
	send_simple_action(url, action_name, value, null, $el);
	
}

function brightness_up_rgb($el, num, id) {
	var value = parseInt($el.attr("value"));
	var step = parseInt($el.attr("step"));
	var max = parseInt($el.attr("max"));
	var min = parseInt($el.attr("min"));
	
	if(step == 1){
		var range = max - min;
		step =  Math.ceil(range/10);
		var count = value/step;
		value = count * step + min;
		alert(value);
	} 
	
	if (value <= (max - step)) {
		value += step;
	} else {
		value = max;
	}

	var data_obj = new Object();
	data_obj['red'] = colors[num - 1][0];
	data_obj['green'] = colors[num - 1][1];
	data_obj['blue'] = colors[num - 1][2];
	data_obj['brightness'] = parseInt(value);
	data_json = JSON.stringify(data_obj);
	alert(data_json);
	$.ajax({
		url : dev_addr + id,
		type : "PUT",
		dataType : "json",
		contentType : "json",
		data : data_json,
		success : function(data) {
			alert(data);
		},
		error : function(data) {
			alert("Error performing device update");
		}
	});

}

function brightness_down_rgb($el, num, id) {
	var value = parseInt($el.attr("value"));
	var step = parseInt($el.attr("step"));
	var max = parseInt($el.attr("max"));
	var min = parseInt($el.attr("min"));
	
	if(step == 1){
		var range = max - min;
		step =  Math.ceil(range/10);
		var count = value/step;
		value = count * step + min;
		alert(step);
	} 
	
	if (value >= (min + step)) {
		value -= step;
	} else {
		value = min;
	}

	var data_obj = new Object();
	data_obj['red'] = colors[num - 1][0];
	data_obj['green'] = colors[num - 1][1];
	data_obj['blue'] = colors[num - 1][2];
	data_obj['brightness'] = parseInt(value);
	data_json = JSON.stringify(data_obj);
	alert(data_json);
	$.ajax({
		url : dev_addr + id,
		type : "PUT",
		dataType : "json",
		contentType : "json",
		data : data_json,
		success : function(data) {
			alert(data);
		},
		error : function(data) {
			alert("Error performing device update");
		}
	});

}
function brightness_down($el){
	var value = parseInt($el.attr("value"));
	var step =  parseInt($el.attr("step"));
	var max =  parseInt($el.attr("max"));
	var min =  parseInt($el.attr("min"));
	
	if(step == 1){
		var range = max - min;
		step =  Math.ceil(range/10);
		var count = value/step;
		value = count * step + min;
		alert(step);
	} 
	
	
	if(value>=(min + step)){
		value -= step;
	}else{
		value = min;
	}
	var url = $el.attr("url");
	var action_name = $el.attr("action_name");
	
	send_simple_action(url, action_name, value, null, $el);
}

function update_brightness(data){
	var info = $("#left_info_"+data.id);
	var brightness = info.find(".brightness");
	
	if(brightness.length){
		var max =  parseInt(brightness.attr("max"));
		var min =  parseInt(brightness.attr("min"));
		var brightnessPercent = 100 * (data.state.brightness/(max - min));
		var value = parseInt(data.state.brightness);
		if(value == max){
			brightness.find("img").css("margin-left", 0);
		}else if((value == max)){
			brightness.find("img").css("margin-left", 0);
		}
		brightness.attr("value", data.state.brightness);
		brightness.css("width", brightnessPercent+"%");
		
		if(typeof data['actions info'].blue != 'undefined'){
			var colorPos = getColorPos(data);
			$("#left_info_" + data.id).find(".color").removeAttr("active_color");
			$("#left_info_" + data.id).find(".color" + (colorPos + 1)).attr("active_color", "active_color");
			$("#left_info_" + data.id).attr("last_num", (colorPos + 1));
		}
	}
}
function setFloorplans(url){
	$.getJSON(url, function(data) {
		$.each(data, function(key, val) {
			if(floorplans === null){
				floorplans = new Object();
			}
			floorplanImgs[key] = new Image();
			floorplanImgs[key].src = val;
			floorplanImgs[key].onerror = function(){this.src = "";this.src = val;alert("img error:"+val);};
			floorplans[key] = val;
			alert(key+ ' '  + val);
		});
	});
}
function showRooms(){
	prev_section = current_section;
	$(".mainSection").hide();
	$("#rooms").show();
	$("#bottombar").show();
	current_section = 'rooms';
	$("#device_info").hide();
	$("#left_device_info").hide();
	$(".sectionLabel").removeAttr("active");
	$("#rooms_label").attr("active", "active");
}
function showDevices(){
	prev_section = current_section;
	showRoom($("#rooms").find(".selected").attr("id"));
	$(".mainSection").hide();
	$("#devices").show();
	//$("#bottombar").hide();
	current_section = 'devices';
	$("#device_info").show();
	$("#left_device_info").show();
	$(".sectionLabel").removeAttr("active");
	$("#devices_label").attr("active", "active");
}
function showFavs(){
	prev_section = current_section;
	$(".mainSection").hide();
	$("#favorites").show();
//	$("#bottombar").hide();
	current_section = 'favorites';
	$(".sectionLabel").removeAttr("active");
	$("#favorites_label").attr("active", "active");
	$("#device_info").hide();
	$("#left_device_info").hide();
	if(favouriteIds && favouriteIds.length){
		console.log(favouriteIds);
		$("#device_info").show();
		$("#left_device_info").show();
		alert($("#favorites > ul > li.selected").attr("id"));
		if($("#favorites > ul > li.selected").length){
			showDevice($("#favorites > ul > li.selected").attr("id").replace("fav_", ""), "favorites");
		}
	}else{
		$("#device_info").hide();
		$("#left_device_info").hide();
	}
}
function showScenes(){
	prev_section = current_section;
	$(".mainSection").hide();
//	$("#bottombar").hide();
	$("#scenes").show();
	current_section = 'scenes';
	$("#device_info").hide();
	$("#left_device_info").hide();
	$(".sectionLabel").removeAttr("active");
	$("#scenes_label").attr("active", "active");
}
function prevRoom(moveBy){
	if(typeof moveBy === 'undefined'){
		moveBy = 1;
	}
	var previous = $("#rooms").find(".selected");
	while(previous.length && moveBy != 0){
		previous = previous.prev();
		moveBy --;
	}
	if(previous.length){
		$("#rooms").find(".selected").removeClass("selected");
		previous.addClass("selected");
		showRoom(previous.attr("id"));
	}
}
function prevDevice(moveBy){
	if(typeof moveBy === 'undefined'){
		moveBy = 1;
	}
	var previous = $("#devices").find(".selected");
	while(previous.length && moveBy != 0){
		previous = previous.prev();
		moveBy --;
	}
	if(previous.length){
		 $("#devices").find(".selected").removeClass("selected");
		previous.addClass("selected");
		showDevice(previous.attr("id"), "devices");
	}
}
function prevFavDevice(moveBy){
	if(typeof moveBy === 'undefined'){
		moveBy = 1;
	}
	var previous = $("#favorites").find(".selected");
	while(previous.length && moveBy != 0){
		previous = previous.prev();
		moveBy --;
	}
	if(previous.length){
		 $("#favorites").find(".selected").removeClass("selected");
		previous.addClass("selected");
		showDevice(previous.attr("id").replace("fav_", ""), "favorites");
	}
}
function prevScene(moveBy){
	if(typeof moveBy === 'undefined'){
		moveBy = 1;
	}
	var previous = ("#scenes").find(".selected");
	while(previous.length && moveBy != 0){
		previous = previous.prev();
		moveBy --;
	}
	if(previous.length){
		 $("#scenes").find(".selected").removeClass("selected");
		previous.addClass("selected");
		showScene(previous.attr("id").replace("scn_", ""));
	}
}
function nextRoom(moveBy){
	if(typeof moveBy === 'undefined'){
		moveBy = 1;
	}
	var next = $("#rooms").find(".selected");
	while(next.length && moveBy != 0){
		next = next.next();
		moveBy--;
	}
	if(next.length){
		$("#rooms").find(".selected").removeClass("selected");
		next.addClass("selected");
		showRoom(next.attr("id"));
	}
}
function nextDevice(moveBy){
	if(typeof moveBy === 'undefined'){
		moveBy = 1;
	}
	var next = $("#devices").find(".selected");
	while(next.length && moveBy != 0){
		next = next.next();
		moveBy--;
	}
	if(next.length){
		$("#devices").find(".selected").removeClass("selected");
		next.addClass("selected");
		showDevice(next.attr("id"), "devices");
	}
}
function nextFavDevice(moveBy){
	if(typeof moveBy === 'undefined'){
		moveBy = 1;
	}
	var next = $("#favorites").find(".selected");
	while(next.length && moveBy != 0){
		next = next.next();
		moveBy--;
	}
	if(next.length){
		$("#favorites").find(".selected").removeClass("selected");
		next.addClass("selected");
		showDevice(next.attr("id").replace("fav_", ""), "favorites");
	}
}
function nextScene(){
	if(typeof moveBy === 'undefined'){
		moveBy = 1;
	}
	var next = $("#scenes").find(".selected");
	while(next.length && moveBy != 0){
		next = next.next();
		moveBy--;
	}
	if(next.length){
		 $("#scenes").find(".selected").removeClass("selected");
		next.addClass("selected");
		showScene(next.attr("id").replace("scn_", ""));
	}
}
function numPressed(num){
	if(current_section=="scenes" || current_section=="rooms")
		return;
	
	var $devices = $("#devices");
	if($devices.find("#" + current_device + "[dev_type='rgb light']").length){
		setRgb(num, current_device);
	}
}
function setRgb(num, id){
	var data_obj = new Object();
	last_num = num;
	data_obj['red'] = colors[num-1][0];
	data_obj['green'] = colors[num-1][1];
	data_obj['blue'] = colors[num-1][2];
	var brightnessVal = $("#left_info_"+id).find(".brightness").attr("value");
	data_obj['brightness'] =  parseInt(brightnessVal);
	data_json = JSON.stringify(data_obj);
	alert(data_json);
	$.ajax({  
		  url:dev_addr + id,  
		  type: "PUT",  
		  dataType: "json",  
		  contentType: "json",  
		  data: data_json,  
		  success: function(data){  
			  alert(data);
		  },  
		  error: function(data){  
		    alert("Error performing device update");  
		  }  
		});
}
function getColorPos(data){
	var diff=99999999;
	var colorPos = 0;
	for(var i=0;i<9;i++){
		var df = Math.sqrt(Math.pow((parseInt(data.state.red) - colors[i][0]), 2) + Math.pow((parseInt(data.state.green) - colors[i][1]), 2) + Math.pow((parseInt(data.state.blue) - colors[i][2]), 2));
		if(df < diff){
			diff = df;
			colorPos = i;
		}
	}
	alert("closest:"+colorPos)
	return colorPos;
}
function isDeviceFavourite(id){
	if(favouriteIds.indexOf(id)!==-1)
		return true;
	else
		return false;
}
function loadScenes(){
	var $scenes = $("#scenes > ul");
	$.getJSON(scenesAddr, function(data) {
		var first_scene = true;
		if(!data){
       	 $("#scenes_header").text(LNG_DATA_FAILED);
       	 return;
       }
		$.each(data, function(key, val) {
			$.getJSON(val, function(data_scene) {
				$scenes.append('<li dev_type="scene" class="devId ' + (first_scene ? ' selected ' : '') +'" id="scn_' + data_scene.id + '"  ><div class="device_label">' + data_scene.label + '</div><div dev_type="scene" class="device_image"></div></li>')
				if(first_scene){
					$("#scenes_header").text(data_scene.label);
				}
				first_scene = false;
			});
		});
		if(first_scene)
			$("#scenes_header").text(LNG_NO_SCENES);
	});
	
}
function sendScene(){
	var $scene = $("#scenes").find(".selected");
	var id = $scene.attr("id").replace("scn_", "");
	var url = scenesAddr + "/"+id;
	$.ajax({  
		  url:url,  
		  type: "PUT"  
	});
}
function drawFloorplan(key, image){
	if(floorplans === null){
		setTimeout(function(){drawFloorplan(key, image);}, 100);
		return;
	}else{
		$("#plan_"+key).css('background-image', 'url("' + floorplans[image] + '")');
	}
}
function previousSection(){
	if(prev_section != current_section){
		switch(prev_section){
			case 'rooms':
			showRooms();
			break;
			case 'devices':
			showDevices();
			break;
			case 'favorites':
			showFavs();
			break;
			case 'scenes':
			showScenes();
			break;
		}
	}
}