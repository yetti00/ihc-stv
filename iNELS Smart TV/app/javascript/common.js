var colors = [[255,255,255],[255, 10, 30], [255,5,255],[75,10,255],[0,145,255],[10,255,145],[0,219,0],[255,255,6],[255,163,0]];

var languages = new Array();
languages.push('en');
languages.push('cs');


function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    console.log('Query variable %s not found', variable);
}
function setFile(fileName, value){
	var fileSystemObj = new FileSystem();
	var filePath = curWidget.id + '/' + fileName;
	alert(filePath);
	var fileObj = fileSystemObj.openCommonFile(filePath, 'w');
	if(!fileObj){

		var bValid = fileSystemObj.isValidCommonPath(curWidget.id);
		if (!bValid) {
			fileSystemObj.createCommonDir(curWidget.id);    
		}

	}

	fileObj = fileSystemObj.openCommonFile(filePath, 'w');

	fileObj.writeLine(value);
	fileSystemObj.closeCommonFile(fileObj);
}

function getFile(fileName){
	var fileSystemObj = new FileSystem();
	var filePath = curWidget.id+'/' + fileName;
	alert(filePath);
	var fileObj = fileSystemObj.openCommonFile(filePath, 'r');
	if(!fileObj){

		var bValid = fileSystemObj.isValidCommonPath(curWidget.id);
		if (!bValid) {
			fileSystemObj.createCommonDir(curWidget.id);    
		}

	}

	fileObj = fileSystemObj.openCommonFile(filePath, 'r');
	var value = null;
	if(fileObj){
		value = fileObj.readLine();
		fileSystemObj.closeCommonFile(fileObj);
	}
	return value;
}

Array.prototype.remove = function(from, to) {
	  var rest = this.slice((to || from) + 1 || this.length);
	  this.length = from < 0 ? this.length + from : from;
	  return this.push.apply(this, rest);
	};
	
var lang = "en";
alert(window.location);
if(typeof window.location.search != 'undefined'){
	lang = getQueryVariable("lang");
	if(typeof lang == 'undefined' || languages.indexOf(lang)==-1){
		lang = "en";
	}
}

$.sf.loadJS("lang/"+lang+".js");