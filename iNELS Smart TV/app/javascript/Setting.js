var widgetAPI = new Common.API.Widget();
var tvKey = new Common.API.TVKeyValue();
var retBack = false;
var first_room = true;
var first_device = true;
var devices = new Array();
var devicesObj = new Object();
var apiAddr = "http://" + apiServer + "/api";
var addr = "http://" + apiServer + "/api/rooms";
var dev_addr = "http://" + apiServer + "/api/devices/";
var scenesAddr = apiAddr + "/scenes";
var floorplanAddr = apiAddr + "/floorplans/";
var strFavIds = getFile("favIds");
var favouriteIds = JSON.parse(strFavIds);
if(!favouriteIds){
	favouriteIds = new Array();
}
var MAX_SCENE_DEVICES = 8;
var sceneContext = "right";
var sceneSubContext = "edit";
first_scene_device = true;
var configContext = "top";
var configMainContext = "devices";
var configDevicesContext = "devices";
var configRoomsContext = "rooms";
var configPairContext = "rooms";
var configDevicesIsNew = false;
var configRoomIsNew = false;
var roomsDevices = new Object();
var camIds = new Array();

var otherContext = "top";
var otherMainContext = "ip";
var newCamera = false;

var max_width = 510;
var max_height = 322;


var Main =
{

};

function renderTranslations(){
	$("#rooms_label").text(LNG_FAVOURITES);
	$("#devices_label").text(LNG_SCENES);
	$("#favorites_label").text(LNG_CONFIGURATION);
	$("#scenes_label").text(LNG_OTHERS);
	$("#sett_favorites_left_label").text(LNG_FAVOURITES_SELECT_DEVICES);
	$("#sett_favorites_right_label").text(LNG_FAVOURITES_SELECTED_DEVICES);
	$("#sett_scenes_left_name_label").text(LNG_SCENE_NAME);
	$("#sett_scenes_left_devices_label").text(LNG_SCENE_DEVICES);
	$("#sett_scenes_left_info_label").text(LNG_SCENE_DEVICE_DESCRIPTION);
	$("#sett_scenes_right_label").text(LNG_SCENE_SELECT_DEVICES);
	$("#sett_room_config_top_devices_label").text(LNG_SETTINGS_DEVICES);
	$("#sett_room_config_top_rooms_label").text(LNG_SETTINGS_ROOMS);
	$("#sett_room_config_top_floorplans_label").text(LNG_SETTINGS_FLOORPLAN);
	$("#sett_room_config_top_pair_label").text(LNG_SETTINGS_PAIR);
	$("#sett_room_config_top_elan_settings").text(LNG_SETTINGS_ELAN);
	$("#sett_room_config_top_cameras").text(LNG_CAMERAS);
	$("#app_version").text(LNG_SETTINGS_APP_VERSION + " 0.132");
	$("#elan_address").text(LNG_SETTINGS_ELAN_ADDRESS);
	$("#sett_room_config_bottom_devices_cams_name_label").text(LNG_SETTINGS_CAMERA_NAME);
	$("#sett_room_config_bottom_devices_cam_addr_label").text(LNG_SETTINGS_CAMERA_ADDRESS);
	$("#sett_room_config_bottom_devices_cam_type_label").text(LNG_SETTINGS_CAMERA_TYPE);
	$("#sett_room_config_bottom_devices_cams_login_label").text(LNG_SETTINGS_CAMERA_USERNAME);
	$("#sett_room_config_bottom_devices_cams_pass_label").text(LNG_SETTINGS_CAMERA_PASSWORD);
	$("#sett_room_config_bottom_devices_cams_right_devices_label").text(LNG_SETTINGS_CAMERA_DEVICES_TITLE);
	$("#sett_room_config_bottom_devices_name_label").text(LNG_SETTINGS_DEVICE_NAME);
	$("#sett_room_config_bottom_devices_actor_addr_label").text(LNG_SETTINGS_DEVICE_ADDRESS);
	$("#sett_room_config_bottom_devices_actor_name_label").text(LNG_SETTINGS_DEVICE_ACTOR_TYPE);
	$("#sett_room_config_bottom_devices_device_type_label").text(LNG_SETTINGS_DEVICE_TYPE);
	$("#sett_room_config_bottom_devices_right_devices_label").text(LNG_SETTINGS_DEVICES_TITLE);
	$("#sett_room_config_bottom_room_name_label").text(LNG_SETTINGS_ROOM_NAME);
	$("#sett_room_config_bottom_room_floorplan_label").text(LNG_SETTINGS_PICK_FLOORPLAN);
	$("#sett_room_config_bottom_room_rooms_label").text(LNG_SETTINGS_ROOMS_TITLE);
	$("#sett_room_config_bottom_room_right_floorplan_label").text(LNG_SETTINGS_FLOORPLAN_PREVIEW);	
	$("#sett_room_config_bottom_floorplan_right_label").text(LNG_SETTINGS_FLOORPLAN_PREVIEW);
	$("#sett_room_config_bottom_floorplan_left_label").text(LNG_SETTINGS_FLOORPLANS_TITLE);
	$("#sett_room_config_bottom_pair_left_label1").text(LNG_SETTINGS_PAIR_PICK_ROOM);
	$("#sett_room_config_bottom_pair_left_label2").text(LNG_SETTINGS_PAIR_DEVICE);
	$("#sett_room_config_bottom_pair_right_label").text(LNG_SETTINGS_PAIR_SETUP_DEVICE_POSITION);
	
}

Main.onLoad = function()
{
	// Enable key event processing
	this.enableKeys();
	widgetAPI.sendReadyEvent();
	renderTranslations();
	$("#bottombar").sfKeyHelp({
		'user': LNG_FAVOURITES_HELP,
	});
	insertABC(); 
	
	var apiServerArr = apiServer.split(/[\.:]/);
	$("#apiServer1").val(apiServerArr[0]);
	$("#apiServer2").val(apiServerArr[1]);
	$("#apiServer3").val(apiServerArr[2]);
	$("#apiServer4").val(apiServerArr[3]);
	$("#apiServer5").val(apiServerArr[4]);
	
	document.getElementById("anchor").focus();
	renderSceneDevices(addr);
	loadConfigRoomsDevices();
	loadConfigRoomsDeviceTypes();
	loadConfigRoomsProductTypes();
	loadConfigFloorplans();
	loadConfigRooms();
	
	insertDevAbc();
	insertDevHexAbc();
	insertRoomsAbc();
	
	loadCams();
	insertCamsAbc();
	camsInsertTypes();
	
	retBack = true;
};

Main.onUnload = function()
{

};

Main.enableKeys = function()
{
	document.getElementById("anchor").focus();
};

Main.keyDown = function(event, _this)
{
	var keyCode = event.keyCode;
	alert("Key pressed: " + keyCode);

	switch(keyCode)
	{
		case tvKey.KEY_EXIT:
			saveApiServer();
			saveFavourites();
			$.sf.exit();
			widgetAPI.sendExitEvent();
			return true;
		break;
		case tvKey.KEY_RETURN:
		//case tvKey.KEY_PANEL_RETURN:
			alert("RETURN");
			//widgetAPI.sendReturnEvent();
			if(retBack){
				if($("#sett_scenes_right:visible").length){
					if(sceneContext == "keyboard"){
						hideKeyboard();
					}else if(sceneContext == "config"){
						hideDeviceConfig();
					}else{
						saveApiServer();
						saveFavourites();
						window.location.href="index.html" + window.location.search;
					}
				}else if($("#sett_room_config_top:visible").length){
					if(configContext == "top"){
						saveApiServer();
						saveFavourites();
						window.location.href="index.html" + window.location.search;
					}else if(configContext == "bottom"){
						if(configMainContext == "devices"){
							if(configDevicesContext == "abc"){
								hideConfigDevicesAbc();
							}else if(configDevicesContext == "actor names"){
								hideConfigDevicesActorNames();
							}else if(configDevicesContext == "dev types"){
								hideConfigDevicesDeviceTypes();
							}else if(configDevicesContext == "left"){
								switchConfigDevicesToRight();
							}else if(configDevicesContext == "hexabc"){
								hideConfigDevicesHexAbc();
							}else{
								$("#sett_room_config_bottom_devices_right_devices").find(".selected").removeClass("selected").addClass("lastSelected");
								configContext = "top";
							}
						}else if(configMainContext == "floorplan"){
							configContext = "top";
						}else if(configMainContext == "rooms"){
							if(configRoomsContext == "rooms"){
								configContext = "top";
							}else if(configRoomsContext == "floorplans"){
								cancelConfigRoomsFloorplan();
								hideConfigRoomsFloorplan();
							}else if(configRoomsContext == "top"){
								configRoomsTopLeave();
							}else if(configRoomsContext == "abc"){
								hideConfigRoomsAbc();
							}
						}else if(configMainContext == "pair"){
							if(configPairContext == "rooms"){
								configContext = "top";
								var $rooms = $("#sett_room_config_bottom_pair_left_rooms");
								$rooms.find(".selected").removeClass("selected").addClass("lastSelected");
							}else if(configPairContext == "floorplans"){
								configPairDeviceFloorplanCancel();
							}else if(configPairContext == "devices"){
								
							}
						}
						
					}
				}else if($("#sett_other_config:visible").length){
					alert(otherContext);
					if(otherContext == "top"){
						saveApiServer();
						saveFavourites();
						window.location.href="index.html" + window.location.search;
					}else if(otherContext == "bottom"){
						if(otherMainContext == "ip" || otherMainContext == "devices"){
							otherContext = "top";
							$(document.activeElement).blur();
							$("#anchor").focus();
							camsSwitchFromLeftToTop();
						}else if(otherMainContext == "left"){
							if($("#sett_room_config_bottom_devices_cams_right_devices").find("li").length){
								camsSwitchFromLeftToDevices();
							}else{
								camsSwitchFromLeftToTop();
							}
						}else if(otherMainContext == "abc"){
							camsSwitchFromAbcToLeft();
						}else if(otherMainContext == "types"){
							camsSwitchFromTypesToLeft();
						}
					}
				}else{
					saveApiServer();
					saveFavourites();
					window.location.href="index.html" + window.location.search;
				}
			}
			break;
		case tvKey.KEY_LEFT:
			alert("LEFT");
			if($("#sett_favorites:visible").length){
				prevDevice();
			}else if($("#sett_scenes_right:visible").length){
				if(sceneContext == "right"){
					prevSceneDevice();
				}else if(sceneContext == "left" && sceneSubContext == "devices"){
					leftDevicesLeft();
				}else if(sceneContext == "keyboard"){
					keyboardLeft();
				}else if(sceneContext == "config"){
					prevAction();
				}
			}else if($("#sett_room_config_top:visible").length){
				if(configContext == "top"){
					prevRoomConfig();
				}else if(configContext == "bottom"){
					if(configMainContext == "devices"){
						if(configDevicesContext == "devices"){
							prevLi($("#sett_room_config_bottom_devices_right_devices").find(".selected"), 1, configRoomsDevicesShowDevice);
						}else if(configDevicesContext == "actor names"){
							configDevicesPrevActor();
						}else if(configDevicesContext == "dev types"){
							configDevicesPrevType();
						}else if(configDevicesContext == "left"){
							configDevicesPrevEdit();
						}else if(configDevicesContext == "abc"){
							configDevicesAbcPrev();
						}else if(configDevicesContext == "hexabc"){
							configDevicesHexAbcPrev();
						}
					}else if(configMainContext == "floorplan"){
						configFloorplanPrev();
					}else if(configMainContext == "rooms"){
						if(configRoomsContext == "rooms"){
							configRoomsPrev();
						}else if(configRoomsContext == "floorplans"){
							configRoomsFloorplansPrev();
						}else if(configRoomsContext == "abc"){
							configRoomsAbcPrev();
						}
					}else if(configMainContext == "pair"){
						if(configPairContext == "devices"){
							switchConfigPairToRooms();
						}else if(configPairContext == "floorplans"){
							configPairDeviceFloorplanLeft();
						}
					}
				}
			}else if($("#sett_other_config_top:visible").length){
				if(otherContext == "top"){
					prevOther();
				}else if(otherContext == "bottom"){
					if(otherMainContext == "ip"){
						$(document.activeElement).prev("input").focus().select();
					}else if(otherMainContext == "left"){
						camsLeftLeft();
					}else if(otherMainContext == "abc"){
						camsAbcPrev();
					}else if(otherMainContext == "types"){
						camsTypesPrev();
					}else if(otherMainContext == "devices"){
						camsDevPrev();
					}
				}
			}else{
				
			}
			break;
		case tvKey.KEY_RIGHT:
			alert(sceneContext +" " +sceneSubContext);
			if($("#sett_favorites:visible").length){
				nextDevice();
			}else if($("#sett_scenes_right:visible").length){
				if(sceneContext == "right"){
					nextSceneDevice();
				}else if(sceneContext == "left" && sceneSubContext == "devices"){
					leftDevicesRight();
				}else if(sceneContext == "keyboard"){
					keyboardRight();
				}else if(sceneContext == "config"){
					nextAction();
				}
			}else if($("#sett_room_config_top:visible").length){
				if(configContext == "top"){
					nextRoomConfig();
				}else if(configContext == "bottom"){
					if(configMainContext == "devices"){
						if(configDevicesContext == "devices"){
							nextLi($("#sett_room_config_bottom_devices_right_devices").find(".selected"), 1, configRoomsDevicesShowDevice);
						}else if(configDevicesContext == "actor names"){
							configDevicesNextActor();
						}else if(configDevicesContext == "dev types"){
							configDevicesNextType();
						}else if(configDevicesContext == "left"){
							configDevicesNextEdit();
						}else if(configDevicesContext == "abc"){
							configDevicesAbcNext();
						}else if(configDevicesContext == "hexabc"){
							configDevicesHexAbcNext();
						}
					}else if(configMainContext == "floorplan"){
						configFloorplanNext();
					}else if(configMainContext == "rooms"){
						if(configRoomsContext == "rooms"){
							configRoomsNext();
						}else if(configRoomsContext == "floorplans"){
							configRoomsFloorplansNext();
						}else if(configRoomsContext == "abc"){
							configRoomsAbcNext();
						}
					}else if(configMainContext == "pair"){
						if(configPairContext == "rooms"){
							switchConfigPairToDevices();
						}else if(configPairContext == "floorplans"){
							configPairDeviceFloorplanRight();
						}
					}
				}
			}else if($("#sett_other_config_top:visible").length){
				if(otherContext == "top"){
					nextOther();
				}else if(otherContext == "bottom"){
					if(otherMainContext == "ip"){
						$(document.activeElement).next("input").focus().select();
					}else if(otherMainContext == "left"){
						camsLeftRight();
					}else if(otherMainContext == "abc"){
						camsAbcNext();
					}else if(otherMainContext == "types"){
						camsTypesNext();
					}else if(otherMainContext == "devices"){
						camsDevNext();
					}
				}
			}else{
				
			}
			alert("RIGHT");
			
			break;
		case tvKey.KEY_UP:
			if($("#sett_favorites:visible").length){
				prevRoom();
			}else if($("#sett_scenes_right:visible").length){
				if(sceneContext == "right"){
					prevSceneRoom();
				}else if(sceneContext == "left" && sceneSubContext == "devices"){
					toLeftEdit();
				}else if(sceneContext == "keyboard"){
					keyboardUp();
				}else if(sceneContext == "config"){
					prevAction();prevAction();
				}
			}else if($("#sett_room_config_top:visible").length){
				if(configContext == "top"){
					
				}else if(configContext == "bottom"){
					if(configMainContext == "devices"){
						if(configDevicesContext == "devices"){
							prevLi($("#sett_room_config_bottom_devices_right_devices").find(".selected"), 2, configRoomsDevicesShowDevice);
						}else if(configDevicesContext == "actor names"){
							configDevicesUpActor();
						}else if(configDevicesContext == "dev types"){
							configDevicesUpType();
						}else if(configDevicesContext == "left"){
							configDevicesUpEdit();
						}else if(configDevicesContext == "abc"){
							configDevicesAbcUp();
						}else if(configDevicesContext == "hexabc"){
							configDevicesHexAbcUp();
						}
					}else if(configMainContext == "floorplan"){
						configFloorplanUp();
					}else if(configMainContext == "rooms"){
						if(configRoomsContext == "rooms"){
							configRoomsUp();
						}else if(configRoomsContext == "floorplans"){
							configRoomsFloorplansUp();
						}else if(configRoomsContext == "top"){
							configRoomsTopUp();
						}else if(configRoomsContext == "abc"){
							configRoomsAbcUp();
						}
					}else if(configMainContext == "pair"){
						if(configPairContext == "rooms"){
							configPairRoomUp();
						}else if(configPairContext == "devices"){
							configPairDeviceUp();
						}else if(configPairContext == "floorplans"){
							configPairDeviceFloorplanUp();
						}
					}
				}
			}else if($("#sett_other_config_top:visible").length){
				if(otherContext == "top"){
					
				}else if(otherContext == "bottom"){
					if(otherMainContext == "ip"){
						
					}else if(otherMainContext == "left"){
						camsLeftUp();
					}else if(otherMainContext == "abc"){
						camsAbcUp();
					}else if(otherMainContext == "types"){
						camsTypesUp();
					}else if(otherMainContext == "devices"){
						camsDevUp();
					}
				}
			}
			alert("UP");		
			break;
		case tvKey.KEY_DOWN:
			if($("#sett_favorites:visible").length){
				nextRoom();
			}else if($("#sett_scenes_right:visible").length){
				if(sceneContext == "right"){
					nextSceneRoom();
				}else if(sceneContext == "left" && sceneSubContext == "edit"){
					toLeftDevices();
				}else if(sceneContext == "keyboard"){
					keyboardDown();
				}else if(sceneContext == "config"){
					nextAction();nextAction();
				}
			}else if($("#sett_room_config_top:visible").length){
				if(configContext == "top"){
					
				}else if(configContext == "bottom"){
					if(configMainContext == "devices"){
						if(configDevicesContext == "devices"){
							nextLi($("#sett_room_config_bottom_devices_right_devices").find(".selected"), 2, configRoomsDevicesShowDevice);
						}else if(configDevicesContext == "actor names"){
							configDevicesDownActor();
						}else if(configDevicesContext == "dev types"){
							configDevicesDownType();
						}else if(configDevicesContext == "left"){
							configDevicesDownEdit();
						}else if(configDevicesContext == "abc"){
							configDevicesAbcDown();
						}else if(configDevicesContext == "hexabc"){
							configDevicesHexAbcDown();
						}
					}else if(configMainContext == "floorplan"){
						configFloorplanDown();
					}else if(configMainContext == "rooms"){
						if(configRoomsContext == "rooms"){
							configRoomsDown();
						}else if(configRoomsContext == "floorplans"){
							configRoomsFloorplansDown();
						}else if(configRoomsContext == "top"){
							configRoomsTopDown();
						}else if(configRoomsContext == "abc"){
							configRoomsAbcDown();
						}
					}else if(configMainContext == "pair"){
						if(configPairContext == "rooms"){
							configPairRoomDown();
						}else if(configPairContext == "devices"){
							configPairDeviceDown();
						}else if(configPairContext == "floorplans"){
							configPairDeviceFloorplanDown();
						}
					}
				}
			}else if($("#sett_other_config_top:visible").length){
				if(otherContext == "top"){
					
				}else if(otherContext == "bottom"){
					if(otherMainContext == "ip"){
						
					}else if(otherMainContext == "left"){
						camsLeftDown();
					}else if(otherMainContext == "abc"){
						camsAbcDown();
					}else if(otherMainContext == "types"){
						camsTypesDown();
					}else if(otherMainContext == "devices"){
						camsDevDown();
					}
				}
			}
			alert("DOWN");
			
			break;
		case tvKey.KEY_ENTER:
		case tvKey.KEY_PANEL_ENTER:
			if($("#sett_favorites:visible").length){
				switchSelectedFavourite();
			}else if($("#sett_scenes_right:visible").length){
				if(sceneContext == "right"){
					switchSelectedScene();
				}else if(sceneContext == "left" && sceneSubContext == "edit"){
					showKeyboard();
				}else if(sceneContext == "keyboard"){
					keyboardEnter();
				}else if(sceneContext == "left" && sceneSubContext == "devices"){
					showDeviceConfig();
				}else if(sceneContext == "config"){
					switchAction();
				}
			}else if($("#sett_room_config_top:visible").length){
				if(configContext == "top"){
					switchToRoomConfig();
				}else if(configContext == "bottom"){
					if(configMainContext == "devices"){
						if(configDevicesContext == "devices"){
							switchConfigDevicesToLeft();
						}else if(configDevicesContext == "actor names"){
							configDevicesEnterActor();
						}else if(configDevicesContext == "dev types"){
							configDevicesEnterType();
						}else if(configDevicesContext == "left"){
							var $left = $("#sett_room_config_bottom_devices_left");
							var $selected = $left.find(".selected");
							var selectedId = $selected.attr("id");
							if(selectedId == "sett_room_config_bottom_devices_actor_mame"){
								showConfigDevicesActorNames();
							}else if(selectedId == "sett_room_config_bottom_devices_device_type"){
								showConfigDevicesDeviceTypes();
							}else if(selectedId == "sett_room_config_bottom_devices_actor_addr"){
								showConfigDevicesHexAbc();
								
							}else{
								showConfigDevicesAbc();
							}
						}else if(configDevicesContext == "abc"){
							configDevicesAbcEnter();
						}else if(configDevicesContext == "hexabc"){
							configDevicesHexAbcEnter();
						}
					}else if(configMainContext == "rooms"){
						if(configRoomsContext == "rooms"){
							configRoomsEnter();
						}else if(configRoomsContext == "floorplans"){
							approveConfigRoomsFloorplan();
							hideConfigRoomsFloorplan();
						}else if(configRoomsContext == "top"){
							configRoomsTopEnter();
						}else if(configRoomsContext == "abc"){
							configRoomsAbcEnter();
						}
					}else if(configMainContext == "pair"){
						if(configPairContext == "rooms"){
							
						}else if(configPairContext == "floorplans"){
							configPairDeviceFloorplanSave();
						}else if(configPairContext == "devices"){
							switchConfigPairDevice();
						}
						
					}
				}
			}else if($("#sett_other_config_top:visible").length){
				if(otherContext == "top"){
					switchToOtherConfig();
				}else if(otherContext == "bottom"){
					if(otherMainContext == "left"){
						camsLeftEnter();
					}else if(otherMainContext == "abc"){
						camsAbcEnter();
					}else if(otherMainContext == "types"){
						camsTypesEnter();
					}else if(otherMainContext == "devices"){
						camsDevicesEnter();
					}
				}
			}
			
			alert("ENTER");
			break;
		case tvKey.KEY_VOL_UP:
			if($("#sett_scenes_right:visible").length){
				if(sceneContext == "config"){
					brightness_up();
				}
			}
			break;
		case tvKey.KEY_VOL_DOWN:
			if($("#sett_scenes_right:visible").length){
				if(sceneContext == "config"){
					brightness_down();
				}
			}
			break;
		case tvKey.KEY_FF:
			if($("#sett_scenes_right:visible").length){
				if(sceneContext == "left"){
					switchScenesToRight();
				}else if(sceneContext == "config"){
					//brightness_up();
				}
			}else if($("#sett_room_config_top:visible").length){
				if(configMainContext == "devices"){
					switchConfigDevicesToCams();
				}else if(configMainContext == "cams"){
					switchConfigCamsToDevices();
				}
			}
			break;
		case tvKey.KEY_RW:
			if($("#sett_scenes_right:visible").length){
				if(sceneContext == "right"){
					switchScenesToLeft();
				}else if(sceneContext == "config"){
					//brightness_down();
				}
			}
			break;
		case tvKey.KEY_PLAY:
			if($("#sett_scenes_right:visible").length){
				if(sceneContext == "right" || sceneContext == "left"|| sceneContext == "config"|| sceneContext == "keyboard"){
					saveScene();
				}
			}else if($("#sett_room_config_top:visible").length){
				if(configContext == "top"){
					
				}else if(configContext == "bottom"){
					if(configMainContext == "devices"){
						if(configDevicesContext == "left"){
							configDevicesSave();
						}else if(configDevicesContext == "dev types"){
							configDevicesSave();
						}
					}else if(configMainContext == "rooms"){
						if(configRoomsContext == "top"){
							configRoomsSave();
						}
					}else if(configMainContext == "pair"){
						if(configPairContext == "rooms" || configPairContext == "devices"){
							configPairDevicePairSave();
						}
					}
					
				}
			}else if($("#sett_other_config_top:visible").length){
				if(otherContext == "top"){
					
				}else if(otherContext == "bottom"){
					if(otherMainContext == "left" || otherMainContext == "abc"){
						camsSaveNewCam();
					}
				}
			}
			alert("PLAY");
			break;
		case tvKey.KEY_STOP:
			if($("#sett_scenes_right:visible").length){
				if(sceneContext == "right" || sceneContext == "left"|| sceneContext == "config"|| sceneContext == "keyboard"){
					clearScene();
				}
			}else if($("#sett_room_config_top:visible").length){
				if(configContext == "top"){
					
				}else if(configContext == "bottom"){
					if(configMainContext == "devices"){
						if(configDevicesContext == "devices"){
							configDevicesDelete();
						}
					}else if(configMainContext == "floorplan"){
						configFloorplanDelete();
						
					}else if(configMainContext == "rooms"){
						if(configRoomsContext == "rooms"){
							configRoomsDelete();
						}
					}
				}
			}else if($("#sett_other_config_top:visible").length){
				if(otherContext == "top"){
					
				}else if(otherContext == "bottom"){
					if(otherMainContext == "devices"){
						camsDeleteCam();
					}
				}
			}
			break;
		case tvKey.KEY_PAUSE:
			if($("#sett_scenes_right:visible").length){
				
			}else if($("#sett_room_config_top:visible").length){
				if(configContext == "top"){
					
				}else if(configContext == "bottom"){
					if(configMainContext == "devices"){
						if(configDevicesContext == "devices"){
							configDevicesNew();
						}
					}else if(configMainContext == "rooms"){
						if(configRoomsContext == "rooms"){
							configRoomsNew();
						}
					}
				}
			}else if($("#sett_other_config_top:visible").length){
				alert(otherMainContext);
				if(otherContext == "top"){
					
				}else if(otherContext == "bottom"){
					if(otherMainContext == "devices"){
						camsNewCam();
					}
				}
			}
			break;
		case tvKey.KEY_1:
			pressed_num(1, _this);
			break;
		case tvKey.KEY_2:
			pressed_num(2, _this);
			break;
		case tvKey.KEY_3:
			pressed_num(3, _this);
			break;
		case tvKey.KEY_4:
			pressed_num(4, _this);
			break;
		case tvKey.KEY_5:
			pressed_num(5, _this);
			break;
		case tvKey.KEY_6:
			pressed_num(6, _this);
			break;
		case tvKey.KEY_7:
			pressed_num(7, _this);
			break;
		case tvKey.KEY_8:
			pressed_num(8, _this);
			break;
		case tvKey.KEY_9:
			pressed_num(9, _this);
			break;
		case tvKey.KEY_0:
			pressed_num(0, _this);
			break;
		case tvKey.KEY_RED:
			showFavorites();
			break;
		case tvKey.KEY_BLUE:
			showOther();
			break;
		case tvKey.KEY_GREEN:
			showScenes();
			break;
		case tvKey.KEY_YELLOW:
			showRoomConfig();
			break;
		default:
			alert("Unhandled key");
			break;
	}
	event.preventDefault();
	return false;
};
function pressed_num(num, _this){
	if($("#sett_scenes_right:visible").length){
		if(sceneContext == "config"){
			setCurrentRGB(num);
		}
	
	}else if(typeof document.activeElement.selectionStart != 'undefined'){
		if(document.activeElement.selectionStart != document.activeElement.selectionEnd){
			$(document.activeElement).val("");
		}
		var newNum = $(document.activeElement).val() + "" + num;
		var parseNumInt = parseInt(newNum);
		if(_this.id!="apiServer5"){
			if(parseNumInt <= 255 && parseNumInt >= 0){
				$(document.activeElement).val(parseNumInt);
			}
		}else{
			if(parseNumInt < 50000 && parseNumInt > 0){
				$(document.activeElement).val(parseNumInt);
			}
		}
	}
}

function saveApiServer(){
	var apiServerIp = $("#apiServer1").val()+"."+$("#apiServer2").val()+"."+$("#apiServer3").val()+"."+$("#apiServer4").val()+":"+$("#apiServer5").val();
	setFile("apiServer", apiServerIp);
}

function renderSceneDevices(addr){
	$.getJSON(addr, function(data) {
		if(!data){
			$("#bottombar").sfKeyHelp({
				'user': LNG_DATA_FAILED
			});
		}
		$.each(data, function(key, val) {
			
			$.getJSON(val.url, function(data_room) {
				//$("#sett_favorites_left_devices").append('<div  room="' + key + '"" id="devices_' + key + '" class="sett_devices_room" ><div class="sett_room_label">' + data_room["room info"].label + '</div><ul></ul></div>');
				
				$("#sett_scenes_right_devices").append('<div  room="' + key + '"" id="devices_' + key + '" class="sett_devices_room" ><div class="sett_room_label">' + data_room["room info"].label + '</div><ul></ul></div>');
				
				
				$.each(data_room.devices, function(key2, val2) {
					devices.push(key2);
					$.getJSON(dev_addr + key2, function(data_device) {
						devicesObj[key2] = data_device;
						//var checked = isDeviceFavourite(key2);
						var sceneChecked = isSceneFavourite(key2);
						//$("#sett_favorites_left_devices").find("#devices_" + key + " > ul").append('<li dev_type="' + data_device['device info'].type + '" class="devId ' + (first_device ? ' selected ' : '') + '" id="' + key2 + '"  ><div class="device_label">' + data_device['device info'].label + '</div><div class="fav_checked" ' + (checked ? ' checked="checked" ' : '') + '></div><div dev_type="' + data_device['device info'].type + '" class="device_image"></div></li>');
						if(getAction(data_device)){
							$("#sett_scenes_right_devices").find("#devices_" + key + " > ul").append('<li dev_type="' + data_device['device info'].type + '" class="devId ' + (first_scene_device ? ' selected ' : '') + '" id="' + key2 + '"  ><div class="device_label">' + data_device['device info'].label + '</div><div class="fav_checked" ' + (sceneChecked ? ' checked="checked" ' : '') + '></div><div dev_type="' + data_device['device info'].type + '" class="device_image"></div></li>');
							first_scene_device = false;
						}
						if(!$("#sett_scenes_left_devices  ul").find("#" + key2).length){
							//$("#sett_favorites_right_devices  ul").append('<li style="' + (!checked ? 'display:none;' : '') + '" dev_type="' + data_device['device info'].type + '" class="devId " id="right_' + key2 + '"  ><div class="device_label">' + data_device['device info'].label + '</div><div dev_type="' + data_device['device info'].type + '" class="device_image"></div></li>');
							$("#sett_scenes_left_devices  ul").append('<li action="'+getAction(data_device)+'" do="on" style="' + (!sceneChecked ? 'display:none;' : '') + '" dev_type="' + data_device['device info'].type + '" class="devId " id="' + key2 + '"  ><div class="device_label">' + data_device['device info'].label + '</div><div dev_type="' + data_device['device info'].type + '" class="device_image"></div></li>');
							$("#sett_scenes_left_info").append('<div style="display:none;" " dev_type="' + data_device['device info'].type + '" class="scnDevInfo device_info" id="inf_' + key2 + '" >'+
									'<div dev_type="' + data_device['device info'].type + '" class="device_image"></div><div class="scn_inf_device_label">' + LNG_NAME + ': ' + data_device['device info'].label + '</div>'+
									'<div class="gearImage"></div><div class="scn_info_function">' + LNG_FUNCTIONS + ': ' + '<span class="scn_info_function_span">' + getActionText("on", data_device['device info'].type) + '</span></div>'+
									(hasBrightness(data_device) ? '<div class="brightness_div"><div class="brightnessImage"></div><div class="brightnessText">' + LNG_BRIGHTNESS +': ' + '<span class="brightnessText_span"></span>%</div></div>' : '')+
									'</div>');
							
							renderSceneConfig(data_device);
						}
						first_device = false;
					});
				});
				first_room  = false;
			});
		});
		
	});
}
function loadScenes(){
	var $scenesUl = $("#sett_scenes_right_scenes > ul");
	$.getJSON(scenesAddr, function(data) {
		$.each(data, function(key, val){
			$.getJSON(val, function(data_scene) {
				$scenesUl.append("<li ");
			});
		});
	});
}
function showFavorites(){
	$(".sett_section").hide();
	$("#sett_favorites").show();
	$(".sectionLabel").removeAttr("active");
	$("#rooms_label").attr("active", "active");
	document.getElementById("anchor").focus();
	$("#bottombar").sfKeyHelp({
		'return':LNG_FAVOURITES_HELP,
	});
}
function showOther(){
	$(".sett_section").hide();
	$("#sett_other_config").show();
	$(".sectionLabel").removeAttr("active");
	$("#scenes_label").attr("active", "active");
	var $other_conf = $("#sett_other_config");
	var $selected = $other_conf.find(".sett_room_config_top.selected");
	alert($selected.attr("bottom"));
	setupConfigOtherHelp($selected.attr("bottom"));
	//$("input[type='text']:first").keydown(Main.keydown).focus().select();
}
function showScenes(){
	$(".sett_section").hide();
	$("#sett_scenes").show();
	$(".sectionLabel").removeAttr("active");
	$("#devices_label").attr("active", "active");
	document.getElementById("anchor").focus();
	$("#bottombar").sfKeyHelp({
		'play':LNG_SAVE,
		'stop':LNG_DELETE
	});
}
function showRoomConfig(){
	$(".sett_section").hide();
	$("#sett_room_config").show();
	$(".sectionLabel").removeAttr("active");
	$("#favorites_label").attr("active", "active");
	document.getElementById("anchor").focus();
	var $room_conf = $("#sett_room_config");
	var $selected = $room_conf.find(".sett_room_config_top.selected");
	setupConfigRoomsHelp($selected.attr("bottom"));
}
function isDeviceFavourite(id){
	if(favouriteIds.indexOf(id)!==-1)
		return true;
	else
		return false;
}
function switchSelectedFavourite(){
	var $selected = $("#sett_favorites_left_devices li.selected");
	var selId = $selected.attr("id");
	var $favChecked = $selected.find(".fav_checked");
	alert("isChecked" + $favChecked.attr("checked"));
	if($favChecked.attr("checked")){
		
		$("#sett_favorites").find("#right_" + selId).hide();
		$favChecked.removeAttr("checked");
		
	}else{
		$favChecked.attr("checked", "checked");
		$("#sett_favorites").find("#right_" + selId).show();
		
	}
	
}
function prevDevice(){
	var $selected = $("#sett_favorites_left_devices li.selected");
	if($selected.prev("li").length){
		$selected.removeClass("selected");
		$selected.prev("li").addClass("selected");
	}
	scrollFavs();
}
function nextDevice(){
	var $selected = $("#sett_favorites_left_devices li.selected");
	if($selected.next("li").length){
		$selected.removeClass("selected");
		$selected.next("li").addClass("selected");
	}
	scrollFavs();
}
function prevRoom(){
	var $selected = $("#sett_favorites_left_devices li.selected");
	prevLi($selected, 6, function(){});
}
function nextRoom(){
	var $selected = $("#sett_favorites_left_devices li.selected");
	nextLi($selected, 6, function(){});
}
function scrollFavs(){
	$selected = $("#sett_favorites_left_devices li.selected");
	var position = $selected.position();
	var diff = $("#sett_favorites_left_devices").width() - position.top + parseInt($("#sett_favorites_left_devices").find(".sett_devices_room").first().css("margin-top"));
	if(diff < 0){
		$("#sett_favorites_left_devices").find(".sett_devices_room").first().css("margin-top", diff);
	}else{
		$("#sett_favorites_left_devices").find(".sett_devices_room").first().css("margin-top", 0);
	}
}
function saveFavourites(){
	var favIds = new Array();
	$("#sett_favorites_left_devices").find(".devId").each(function(){
		if($(this).find(".fav_checked").attr("checked")){
			favIds.push($(this).attr("id"));
		}
	});
	
	setFile("favIds", JSON.stringify(favIds));
}

//
//Scenes
//
function prevSceneDevice(){
	var $selected = $("#sett_scenes_right_devices li.selected");
	if($selected.prev("li").length){
		$selected.removeClass("selected");
		$selected.prev("li").addClass("selected");
	}
	scrollScenes();
}
function nextSceneDevice(){
	var $selected = $("#sett_scenes_right_devices li.selected");
	if($selected.next("li").length){
		$selected.removeClass("selected");
		$selected.next("li").addClass("selected");
	}
	scrollScenes();
}
function prevSceneRoom(){
	var $selected = $("#sett_scenes_right_devices li.selected");
	if($selected.length){
		var $room = $selected.closest(".sett_devices_room");
		var $room_prev = $room.prev(".sett_devices_room");
		var $room_dev = $room_prev.find("ul > li:first");
		while($room_prev.length && !$room_dev.length ){//Prvni mistnost ve ktere je nejake zarizeni
			$room_prev = $room_prev.prev(".sett_devices_room");
			$room_dev = $room_prev.find("ul > li:first");
		}
		if($room_prev.length){
			$selected.removeClass("selected");
			$room_dev.addClass("selected");
		}
	}else{
		$("#sett_favorites_left_devices li").first().addClass("selected");
	}
	scrollScenes();
}
function nextSceneRoom(){
	var $selected = $("#sett_scenes_right_devices li.selected");
	if($selected.length){
		var $room = $selected.closest(".sett_devices_room");
		var $room_next = $room.next(".sett_devices_room");
		var $room_dev = $room_next.find("ul > li:first");
		while($room_next.length && !$room_dev.length ){//Prvni mistnost ve ktere je nejake zarizeni
			$room_next = $room_next.next(".sett_devices_room");
			$room_dev = $room_next.find("ul > li:first");
		}
		if($room_next.length){
			$selected.removeClass("selected");
			$room_dev.addClass("selected");
		}
	}else{
		$("#sett_scenes_right_devices li").first().addClass("selected");
	}
	scrollScenes();
}
function scrollScenes(){
	$selected = $("#sett_scenes_right_devices li.selected");
	var position = $selected.position();
	var diff = $("#sett_scenes_right_devices").width() - position.top + parseInt($("#sett_scenes_right_devices").find(".sett_devices_room").first().css("margin-top"));
	if(diff < 0){
		$("#sett_scenes_right_devices").find(".sett_devices_room").first().css("margin-top", diff);
	}else{
		$("#sett_scenes_right_devices").find(".sett_devices_room").first().css("margin-top", 0);
	}
}
function isSceneFavourite(id){
	return false;
}
function switchSelectedScene(){
	var $selected = $("#sett_scenes_right_devices li.selected");
	var selId = $selected.attr("id");
	var $favChecked = $selected.find(".fav_checked");;
	if($favChecked.attr("checked")){
		$("#sett_scenes_left_devices").find("#" + selId).hide();
		$("#sett_scenes_right_devices").find(".sett_devices_room").each(function(){
			$(this).find("#" + selId).find(".fav_checked").removeAttr("checked");
		});
	}else{
		var $leftDevices = $("#sett_scenes_left_devices");
		if($leftDevices.find("li:visible").length < MAX_SCENE_DEVICES){
			$favChecked.attr("checked", "checked");
			$("#sett_scenes_left_devices").find("#" + selId).show();
			$("#sett_scenes_right_devices").find(".sett_devices_room").each(function(){
				$(this).find("#" + selId).find(".fav_checked").attr("checked", "checked");
			});
		}
	}
	
}
function getAction(data){
	if(typeof data.state.on!='undefined'){
		return "on";
	}else if(typeof data.state.up!='undefined'){
		return "up";
	}else if(typeof data.state.brightness!='undefined'){
		return "brightness";
	}else if(typeof data.state['roll up']!='undefined'){
		return "roll up";
	}else{
		return false;
	}
}
function switchScenesToLeft(){
	sceneContext = "left";
	sceneSubContext = "edit";
	var $leftDevices = $("#sett_scenes_left_devices");
	var $lis = $leftDevices.find("li");
	var $edit = $("#sett_scenes_left_name_edit");
	$edit.addClass("selected");
	var $rightDevices = $("#sett_scenes_right_devices");
	$rightDevices.find(".selected").attr("lastSelected", "lastSelected").removeClass("selected");
	
}
function switchScenesToRight(){
	sceneContext = "right";
	
	var $rightDevices = $("#sett_scenes_right_devices");
	var $lis = $rightDevices.find("li");
	if($lis.length){
		$("#sett_scenes_left").find(".selected").removeClass("selected");
		var $last = $lis.find("[lastSelected]");
		if($last.length){
			$last.addClass("selected").removeAttr("lastSelected");
		}else{
			$lis.first().addClass("selected");
		}
		$("#sett_scenes_left_info").find("[id^='inf_']").hide();
	}
		
	
}
function insertABC(){
	$target = $("#sett_scenes_right_keyboard > ul");
	var first = true;
	for(var i =97;i<=122;i++){
		var char = String.fromCharCode(i);
		$target.append('<li char="' + char + '" ' + (first ? ' class="selected" ': '')+'>' + char + '</li>');
		first = false;
	}
	for(var i =48;i<=57;i++){
		var char = String.fromCharCode(i);
		$target.append('<li char="' + char + '" ' + (first ? ' class="selected" ': '')+'>' + char + '</li>');
		first = false;
	}
	$target.append('<li char="-">-</li>');
	$target.append('<li char=" ">space</li>');
	$target.append('<li char="shift">shift</li>');
	$target.append('<li char="backspace">back<br>space</li>');
	
}
function showKeyboard(){
	$("#sett_scenes_right_label").hide();
	$("#sett_scenes_right_devices").hide();
	$("#sett_scenes_right_keyboard").show();
	sceneContext = "keyboard";
	
}
function hideKeyboard(){
	$("#sett_scenes_right_label").show();
	$("#sett_scenes_right_devices").show();
	$("#sett_scenes_right_keyboard").hide();
	sceneContext = "left";
	
}
function keyboardLeft(){
	var $keyboard = $("#sett_scenes_right_keyboard");
	var $selected = $keyboard.find(".selected");
	//var $edit = $("#sett_scenes_left_name_edit");
	$prev = $selected.prev("li");
	if($prev.length){
		$selected.removeClass("selected");
		$prev.addClass("selected");
	}
}
function keyboardRight(){
	var $keyboard = $("#sett_scenes_right_keyboard");
	var $selected = $keyboard.find(".selected");
	//var $edit = $("#sett_scenes_left_name_edit");
	$next = $selected.next("li");
	if($next.length){
		$selected.removeClass("selected");
		$next.addClass("selected");
	}
}
function keyboardUp(){
	keyboardLeft();keyboardLeft();keyboardLeft();keyboardLeft();keyboardLeft();keyboardLeft();keyboardLeft();keyboardLeft();keyboardLeft();keyboardLeft();
}
function keyboardDown(){
	keyboardRight();keyboardRight();keyboardRight();keyboardRight();keyboardRight();keyboardRight();keyboardRight();keyboardRight();keyboardRight();keyboardRight();
}
function keyboardEnter(){
	var $abc = $("#sett_scenes_right_keyboard");
	var $selected = $abc.find(".selected");
	var $left = $("#sett_scenes_left_name_edit");
	var $selectedLeft = $("#sett_scenes_left_name_edit");
	var $selectedLeftSpan = $selectedLeft.find("span");
	var letter = $selected.attr("char");
	if(letter == "backspace"){
		$selectedLeftSpan.html($selectedLeftSpan.text().substring(0, $selectedLeftSpan.text().length - 1));
	}else if(letter == "shift"){
		$abc.find("li").each(function(){
			var $this = $(this);
			var currLetter = $this.attr("char");
			if(currLetter.length == 1){
				var charCode = currLetter.charCodeAt(0);
				if(charCode >= 97 && charCode <= 122){
					$this.html(String.fromCharCode(charCode - 32));
					$this.attr("char", String.fromCharCode(charCode - 32));
				}else if(charCode >= 65 && charCode <= 90){
					$this.html(String.fromCharCode(charCode + 32));
					$this.attr("char", String.fromCharCode(charCode + 32));
				}
			}
		});
	}else{
		$selectedLeftSpan.html($selectedLeftSpan.html() + letter);
	}
}
function toLeftDevices(){
	var $edit = $("#sett_scenes_left_name_edit");
	var $devices =  $("#sett_scenes_left_devices");
	var $lis = $devices.find("li:visible");
	if($lis.length){
		$lis.first().addClass("selected");
		sceneSubContext = "devices";
		$edit.removeClass("selected");
		showDevice($lis.first().attr("id").replace("inf_", ""));
	}
}
function toLeftEdit(){
	var $edit = $("#sett_scenes_left_name_edit");
	var $devices =  $("#sett_scenes_left_devices");

	var $selected = $devices.find(".selected");
	$edit.addClass("selected");
	$("#sett_scenes_left_info").find("[id^='inf_']").hide();
	sceneSubContext = "edit";
	$selected.removeClass("selected");
	
}
function leftDevicesLeft(){
	var $devices =  $("#sett_scenes_left_devices");
	var $selected = $devices.find(".selected");
	var $prev =  $selected.prevAll("li:visible:first");
	if($prev.length){
		$prev.addClass("selected");
		$selected.removeClass("selected");
		var id = $prev.attr("id");
		showDevice(id);
	}
}
function leftDevicesRight(){
	var $devices =  $("#sett_scenes_left_devices");
	var $selected = $devices.find(".selected");
	var $next =  $selected.nextAll("li:visible:first");
	if($next.length){
		$next.addClass("selected");
		$selected.removeClass("selected");
		var id = $next.attr("id");
		showDevice(id);
	}
}
function showDevice(id){
	$("#sett_scenes_left_info").find("[id^='inf_']").hide();
	var $info = $("#sett_scenes_left_info").find("#inf_"+id);
	$info.show();
}
function getActionText(action, dev_type){
	if(action == "on"){
		if(dev_type == "blinds"){
			return LNG_UP;
		}else{
			return LNG_TURN_ON;
		}
	}else if(action == "off"){
		if(dev_type == "blinds"){
			return LNG_DOWN;
		}else{
			return LNG_TURN_OFF;
		}
	}else if(action == "brightness"){
		return LNG_BRIGHTNESS;
	}
}
function showDeviceConfig(){
	var $devices = $("#sett_scenes_left_devices");
	var $selectedDevice = $devices.find(".selected");
	var devId = $selectedDevice.attr("id"); 
	
	
	$("#sett_scenes_left_devices").hide();
	$("#sett_scenes_left_config > div").hide();
	
	$("#sett_scenes_left_config").show();
	var $devConfig = $("#sett_scenes_left_config").find("#conf_"+devId);
	$devConfig.show();
	$devConfig.find(".scn_button").removeClass("selected");
	$devConfig.find(".scn_button:first").addClass("selected");
	sceneContext = "config";
	
	$("#bottombar").sfKeyHelp({
		'pause':LNG_NEW_DEVICE,
		'play':LNG_SAVE,
		'stop':LNG_DELETE
	});
}
function hideDeviceConfig(){
	$("#sett_scenes_left_config").hide();
	$("#sett_scenes_left_devices").show();
	sceneContext = "left";
	sceneSubContext = "devices";
}
function renderBrightness(data){
	//$("#device_info").append("<div class='device_action' id='action_" + data.id + "'></div>");
	 
	var html = "";
	if(typeof data['actions info'].brightness != 'undefined'){
		$("#sett_scenes_left_config").find("#conf_" + data.id).append("<div class=''  style='display:block;' id='left_info_" + data.id + "'  dev_type='" + data['device info'].type + "' ><div class='left_info_top' style='display:none;'></div><div class='left_info_bottom'></div></div>");
		
		var brightnessPercent = 100 * (data.state.brightness/(data['actions info'].brightness.max - data['actions info'].brightness.min));
		
		
		html = "<div class='brightness_container_before'></div><div class='brightness_container'>" +
				"<div class='brightness' max='" + data['actions info'].brightness.max  + "' min='" + data['actions info'].brightness.min + "' url='"+ (dev_addr + data.id) +"' action_name='brightness' " +
						"style='width:" + brightnessPercent +"%;' step='" + data['actions info'].brightness.step  + "' value='" + data.state.brightness + "'><img src='images/540/slider_jas-10.png' /></div></div><div class='brightness_container_after'></div>"+
						"<div class='brightness_conf'><div class='brightness_conf_img'></div><div class='brightness_conf_text'>" + LNG_BRIGHTNESS + ": <span class='brightness_conf_text_span'>" + brightnessPercent.toFixed() + "</span>%</div></div>";
		$("#left_info_" + data.id).find(".left_info_bottom").append(html);
		$("#inf_" + data.id).find(".brightnessText_span").text(brightnessPercent.toFixed());

	}
	if(typeof data['actions info'].blue != 'undefined'){
		html = "<div class='color color1' num='1'></div><div class='color color2' num='2'></div><div class='color color3' num='3'></div><div class='color color4' num='4'></div><div class='color color5' num='5'></div>" +
				"<div class='color color6' num='6'></div><div class='color color7' num='7'></div><div class='color color8' num='8'></div><div class='color color9' num='9'></div>";
		$("#left_info_" + data.id + " > .left_info_top").append(html);
		var colorPos = 0;
		$("#left_info_" + data.id).find(".color").removeAttr("active_color");
		$("#left_info_" + data.id).find(".color" + (colorPos + 1)).attr("active_color", "active_color");
		$("#left_info_" + data.id).attr("last_num", (colorPos + 1));
	}
	
}
function nextAction(){
	var $devices = $("#sett_scenes_left_devices");
	var $selectedDevice = $devices.find(".selected");
	var devId = $selectedDevice.attr("id"); 
	
	var $config = $("#sett_scenes_left_config").find("#conf_" + devId);
	var $selected = $config.find(".selected");
	var $next = $selected.next(".scn_button");
	if($next.length){
		$selected.removeClass("selected");
		$next.addClass("selected");
		if($next.attr("type") == "brightness"){
			$config.find(".left_info_top").hide();
			$config.find(".left_info_bottom").show();
		}
		if($next.attr("type") == "rgb"){
			$config.find(".left_info_top").show();
			$config.find(".left_info_bottom").hide();
		}
	}
}
function prevAction(){
	var $devices = $("#sett_scenes_left_devices");
	var $selectedDevice = $devices.find(".selected");
	var devId = $selectedDevice.attr("id"); 
	
	var $config = $("#sett_scenes_left_config").find("#conf_" + devId);
	var $selected = $config.find(".selected");
	var $prev = $selected.prev(".scn_button");
	if($prev.length){
		$selected.removeClass("selected");
		$prev.addClass("selected");
		if($prev.attr("type") == "brightness"){
			$config.find(".left_info_top").hide();
			$config.find(".left_info_bottom").show();
		}
		if($prev.attr("type") == "rgb"){
			$config.find(".left_info_top").show();
			$config.find(".left_info_bottom").hide();
		}
	}
}
function switchAction(){
	var $devices = $("#sett_scenes_left_devices");
	var $selectedDevice = $devices.find(".selected");
	var devId = $selectedDevice.attr("id");
	var dev_type = $selectedDevice.attr("dev_type"); 
	var $config = $("#sett_scenes_left_config").find("#conf_" + devId);
	var $selectedAction = $config.find(".selected");
	
	
	$check2 = $selectedAction.find(".scn_conf_check2");
	if($check2.length){
		if($check2.attr("active")){
			$check2.removeAttr("active");
		}else{
			$check2.attr("active", "active");
		}
		
	}else{
		$config.find(".scn_conf_check").removeAttr("active");
		$selectedAction.find(".scn_conf_check").attr("active", "active");
		var action = $selectedAction.attr("type");
		switchActionTo(devId, action, dev_type);
	}
	
	
}
function switchActionTo(id, action, dev_type){
	var $selected = $("#sett_scenes_left_devices").find("#" + id);
	$selected.attr("do", action);
	var $info = $("#inf_" + id);
	$info.find(".scn_info_function_span").text(getActionText(action, dev_type));
}
function renderSceneConfig(data){
	var html = '';
	html += '<div id="conf_' + data.id + '" style="display:none;">'+
		'<div class="scn_conf_on scn_button selected" type="on"><div class="scn_conf_check" active="active"></div><div class="scn_conf_text">' + getSceneConfigOnLabel(data['device info'].type) + '</div></div>'+
	  	'<div class="scn_conf_off scn_button" type="off"><div class="scn_conf_check"></div><div class="scn_conf_text">' + getSceneConfigOffLabel(data['device info'].type) + '</div></div>';
	if(typeof data['actions info'].brightness != 'undefined'){
		html +='<div class="scn_conf_brightness scn_button"  type="brightness"><div class="scn_conf_check"></div><div class="scn_conf_text">' + LNG_DIMM_DEVICE + '</div></div>';
	}
	if(typeof data['actions info'].blue != 'undefined'){
		html +='<div class="scn_conf_rgb scn_button"  type="rgb"><div class="scn_conf_check2"></div><div class="scn_conf_text">'+ LNG_SETUP_COLOR + '</div></div>';
	}
	html += '</div>';
	$("#sett_scenes_left_config").append(html);
	renderBrightness(data);
}
function getSceneConfigOnLabel(dev_type){
	if(dev_type === "blinds"){
		return LNG_BLINDS_UP;
	}else{
		return LNG_TURN_ON_DEVICE;
	}
}
function getSceneConfigOffLabel(dev_type){
	if(dev_type === "blinds"){
		return LNG_BLINDS_DOWN;
	}else{
		return LNG_TURN_OFF_DEVICE;
	}
}
function brightness_up(){
	var $devices = $("#sett_scenes_left_devices");
	var $selectedDevice = $devices.find(".selected");
	var devId = $selectedDevice.attr("id"); 
	var $el = $("#conf_" + devId).find(".brightness");
	if($el.length){
		var value = parseInt($el.attr("value"));
		var step =  parseInt($el.attr("step"));
		var max =  parseInt($el.attr("max"));
		var min =  parseInt($el.attr("min"));
		
		if(step == 1){
			var range = max - min;
			step =  Math.ceil(range/10);
			var count = value/step;
			value = count * step + min;
		} 
		
		if(value<=(max - step)){
			value += step;
		}else{
			value = max;
		}	
		
		update_brightness(value);
	}
	
}
function brightness_down(){
	var $devices = $("#sett_scenes_left_devices");
	var $selectedDevice = $devices.find(".selected");
	var devId = $selectedDevice.attr("id"); 
	var $el = $("#conf_" + devId).find(".brightness");
	if($el.length){
		var value = parseInt($el.attr("value"));
		var step =  parseInt($el.attr("step"));
		var max =  parseInt($el.attr("max"));
		var min =  parseInt($el.attr("min"));
		
		if(step == 1){
			var range = max - min;
			step =  Math.ceil(range/10);
			var count = value/step;
			value = count * step + min;
		} 
		
		if(value>=(min + step)){
			value -= step;
		}else{
			value = min;
		}
		update_brightness(value);
	}
}
function update_brightness( value){
	var $devices = $("#sett_scenes_left_devices");
	var $selectedDevice = $devices.find(".selected");
	var devId = $selectedDevice.attr("id"); 
	var $config = $("#conf_" + devId);
	$config.find(".scn_conf_check").removeAttr("active");
	$config.find("[type='brightness']").find(".scn_conf_check").attr("active", "active");
	
	var info = $("#left_info_"+devId);
	var brightness = info.find(".brightness");
	
	if(brightness.length){
		var max =  parseInt(brightness.attr("max"));
		var min =  parseInt(brightness.attr("min"));
		var brightnessPercent = 100 * (value/(max - min));
		if(value == max){
			brightness.find("img").css("margin-left", 0);
		}else if((value == max)){
			brightness.find("img").css("margin-left", 0);
		}
		brightness.attr("value", value);
		brightness.css("width", brightnessPercent+"%");

		var percInt = Math.round(brightnessPercent);
		info.find(".brightness_conf_text_span").text(percInt);
		$("#inf_" + devId).find(".brightnessText_span").text(percInt);
		
	}
}
function hasBrightness(data){
	if(typeof data['actions info'].brightness != 'undefined'){
		return true;
	}else{
		return false;
	}
}
function saveScene(){
	$edit = $("#sett_scenes_left_name_edit > span");
	var label = $edit.text();
	if(label == ""){
//		$("#popup").show();
//		setTimeout('$("#popup").hide()',10000);
//		return;
		$("#sceneSaveResult").text("Chybí název!");
		setTimeout('$("#sceneSaveResult").text("")',10000);
		return;
	}
	var objToSend = new Object();
	objToSend.label = label;
	objToSend.actions = new Array();
	$("#sett_scenes_left_devices").find("li").each(function(){
		var $this = $(this);
		if($this.css("display")!="none"){
			var id = $this.attr("id");
			var action = $this.attr("action");
			var toDo = $this.attr("do");
			var obj = new Object();
			var $config = $("#conf_" + id);
			var $rgb = $config.find("[type='rgb']");
			if($rgb.find(".scn_conf_check2").attr("active")){
				var num = $config.find("[active_color]").attr("num");
				obj["red"] = colors[num][0];
				obj["green"] = colors[num][1];
				obj["blue"] = colors[num][2];
			}
			if(action == "brightness"){
				var $brightness = $("#conf_" + id).find(".brightness");
				if(toDo == "on"){
					obj["brightness"] = parseInt($brightness.attr("max"));
				}
				if(toDo == "off"){
					obj["brightness"] = parseInt($brightness.attr("max"));
				}
				if(toDo == "brightness"){
					obj["brightness"] = parseInt($brightness.attr("max"));
				}
			}
			if(action == "on"){
				if(toDo == "on"){
					obj["on"] = true;
				}
				if(toDo == "off"){
					obj["on"] = false;
				}
			}
			if(action == "up"){
				if(toDo == "on"){
					obj["up"] = true;
				}
				if(toDo == "off"){
					obj["up"] = false;
				}
			}
			if(action == "roll up"){
				if(toDo == "on"){
					obj["roll up"] = null;
				}
				if(toDo == "off"){
					obj["roll down"] = null;
				}
			}

			var obj2 = new Object();
			obj2[id] = obj;
			objToSend.actions.push(obj2);
		}
	});
	if(!objToSend.actions.length){
		$("#sceneSaveResult").text("Není nic vybráno!");
		setTimeout('$("#sceneSaveResult").text("")',10000);
		return;
	}
	var data_json = JSON.stringify(objToSend);

	
	var url = scenesAddr;
	$.ajax({  
		  url:url,  
		  type: "POST",
		  dataType: "json",  
		  contentType: "json",  
		  data: data_json,  
		  success: function(data){  
			  $("#sceneSaveResult").text("Uloženo!");
			  setTimeout('$("#sceneSaveResult").text("")',10000);
			  clearScene();
		  },  
		  error: function(data){  
			  $("#sceneSaveResult").text("Chyba ukládání!");
			  setTimeout('$("#sceneSaveResult").text("")',10000);
		  }  
	});
}
function clearScene(){
	$edit = $("#sett_scenes_left_name_edit > span").text("");
	$("#sett_scenes_left_name_edit").removeClass("selected");
	$("#sett_scenes_left_devices").find("li").each(function(){
		var $this = $(this);
		$this.css("display", "none");
		$this.removeClass("selected");
	});
	$("#sett_scenes_right_devices").find(".fav_checked").removeAttr("checked");
	$("#sett_scenes_right_devices").find("li").removeClass("selected");
	$("#sett_scenes_right_devices").find("li:first").addClass("selected");
	sceneContext = "right";
}
function setCurrentRGB(num){
	var $devices = $("#sett_scenes_left_devices");
	var $selectedDevice = $devices.find(".selected");
	var devId = $selectedDevice.attr("id"); 
	var $config = $("#conf_" + devId);
	$config.find("[type='rgb']").find(".scn_conf_check2").attr("active", "active");
	
	$config.find(".color").removeAttr("active_color");
	$config.find(".color" + num).attr("active_color", "active_color");
}

////////////////
//Rooms config
//

function prevRoomConfig(){
	var $room_conf = $("#sett_room_config");
	var $selected = $room_conf.find(".sett_room_config_top.selected");
	var $next = $selected.prev(".sett_room_config_top");
	if($next.length){
		var bottomId = $next.attr("bottom");
		$room_conf.find(".sett_room_config_top").removeClass("selected");
		$next.addClass("selected");
		$room_conf.find(".sett_room_config_bottom_section").hide();
		$("#" + bottomId).show();
		setupConfigRoomsHelp(bottomId);
	}
}
function nextRoomConfig(){
	var $room_conf = $("#sett_room_config");
	var $selected = $room_conf.find(".sett_room_config_top.selected");
	var $next = $selected.next(".sett_room_config_top");
	if($next.length){
		var bottomId = $next.attr("bottom");
		$room_conf.find(".sett_room_config_top").removeClass("selected");
		$next.addClass("selected");
		$room_conf.find(".sett_room_config_bottom_section").hide();
		$("#" + bottomId).show();
		setupConfigRoomsHelp(bottomId);
	} 
}

function setupConfigRoomsHelp(bottomId){
	if(bottomId == "sett_room_config_bottom_devices"){
		$("#bottombar").sfKeyHelp({
			'pause':LNG_NEW_DEVICE,
			'play':LNG_SAVE,
			'stop':LNG_DELETE
		});
	} else if (bottomId == "sett_room_config_bottom_room"){
		$("#bottombar").sfKeyHelp({
			'pause':LNG_NEW_ROOM,
			'play':LNG_SAVE,
			'stop':LNG_DELETE
		});
	} else if (bottomId == "sett_room_config_bottom_floorplan"){
		$("#bottombar").sfKeyHelp({
			'stop':LNG_DELETE
		});
	} else if (bottomId == "sett_room_config_bottom_pair"){
		$("#bottombar").sfKeyHelp({
			'play':LNG_SAVE
		});
	}
}

function setupConfigOtherHelp(bottomId){
	if(bottomId == "ip_setting"){
		$("#bottombar").sfKeyHelp({
			'return':LNG_SAVE_AFTER_EXIT
		});
	} else if (bottomId == "sett_room_config_bottom_devices_cams"){
		$("#bottombar").sfKeyHelp({
			'pause':LNG_NEW_ROOM,
			'play':LNG_SAVE,
			'stop':LNG_DELETE
		});
	} 
}

function loadConfigRoomsDeviceTypes(){
	var first = true;
	$devTypes = $("#sett_room_config_bottom_devices_right_dev_types > ul");
	var url = apiAddr + "/configuration/device_types";
	$.getJSON(url, function(data) {
		$.each(data, function(key, val) {	
			$devTypes.append("<li dev_type='" + key + "' class='" + (first ? "selected":"") + "' >" + key + "</li>");
			first = false;
		});
	});
}
function loadConfigRoomsProductTypes(){
	var first = true;
	$prodTypes = $("#sett_room_config_bottom_devices_right_prod_types > ul");
	var url = apiAddr + "/configuration/product_types";
	$.getJSON(url, function(data) {
		$.each(data, function(key, val) {	
			$prodTypes.append("<li prod_type='" + val + "' class='" + (first ? "selected":"") + "' >" + val + "</li>");
			first = false;
		});
	});
}
function loadConfigRoomsDevices(){
	var first = true;
	$devices = $("#sett_room_config_bottom_devices_right_devices > ul");
	$("#sett_favorites_left_devices > ul").html("");
	$("#sett_favorites_right_devices  ul").html("");
	$devices.html("");
	$.getJSON(dev_addr, function(data) {
		
		$.each(data, function(key, val) {	

			$.getJSON(val.url, function(data_dev) {
				var html = "<li dev_id='" + data_dev.id + "' class='" + (first ? "selected":"") + "' ";
				$.each(data_dev['device info'], function(key2, val2) {
					html += " " + key2.replace(" ", "_") + "='" + val2 + "' ";
				});
				html += " dev_addr='" + data_dev['device info'].address.toString(16) + "'>" + data_dev['device info'].label + "</li>";
				$devices.append(html);
				
				var checked = isDeviceFavourite(key);
				$("#sett_favorites_left_devices > ul").append('<li dev_type="' + data_dev['device info'].type + '" class="devId ' + (first ? ' selected ' : '') + '" id="' + key + '"  ><div class="device_label">' + data_dev['device info'].label + '</div><div class="fav_checked" ' + (checked ? ' checked="checked" ' : '') + '></div><div dev_type="' + data_dev['device info'].type + '" class="device_image"></div></li>');
				$("#sett_favorites_right_devices  ul").append('<li style="' + (!checked ? 'display:none;' : '') + '" dev_type="' + data_dev['device info'].type + '" class="devId " id="right_' + key + '"  ><div class="device_label">' + data_dev['device info'].label + '</div><div dev_type="' + data_dev['device info'].type + '" class="device_image"></div></li>');
				first = false;
				
				$selected = $("#sett_room_config_bottom_devices_right_devices").find(".selected");
				  configRoomsDevicesShowDevice($selected);
			});
		});
	});
}
function switchToRoomConfig(){
	configContext = "bottom";
	$selected = $("#sett_room_config_top").find(".selected");
	if($selected.attr("id") == "sett_room_config_top_devices"){
		switchToConfigRoomsDevices();
	}else if($selected.attr("id") == "sett_room_config_top_floorplan"){
		switchToConfigRoomsFloorplan();
	}else if($selected.attr("id") == "sett_room_config_top_room"){
		switchToConfigRoomsRooms();
	}else if($selected.attr("id") == "sett_room_config_top_pair"){
		switchToConfigRoomsPair();
	}
}
function switchToOtherConfig(){
	configContext = "bottom";
	$selected = $("#sett_other_config_top").find(".selected");
	if($selected.attr("id") == "sett_other_config_top_ip"){
		switchToOtherIp();
	}else if($selected.attr("id") == "sett_other_config_top_cams"){
		switchToOtherCams();
	}
}
function switchToConfigRoomsDevices(){
	
	configMainContext = "devices";
	configDevicesContext = "devices";
	$devices = $("#sett_room_config_bottom_devices_right_devices > ul");
	$select = $devices.find(".lastSelected").removeClass("lastSelected");
	if(!$select.length){
		$select = $devices.find("li").first();
	}
	$select.addClass("selected");
	configRoomsDevicesShowDevice($select);
}
function configRoomsDevicesShowDevice($el){
	$label = $("#sett_room_config_bottom_devices_name > span");
	$image = $("#sett_room_config_bottom_devices_image");
	$actorAddr = $("#sett_room_config_bottom_devices_actor_addr > span");
	$actorName = $("#sett_room_config_bottom_devices_actor_mame > span");
	$actorDevType = $("#sett_room_config_bottom_devices_device_type > span");
	
	$label.text($el.attr("label"));
	$image.attr("dev_type", $el.attr("type"));
	$actorAddr.text($el.attr("dev_addr"));
	$actorName.text($el.attr("product_type"));
	$actorDevType.text($el.attr("type"));
	
	configDevicesIsNew = false;
}
function nextLi($current, moveBy, callback){
	var $origCurrent = $current;
	for(var i=0;i<moveBy;i++){
		var $next = $current.next("li");
		if($next.length){
			$current = $next;
		}
	}
	if($next.length){
		$origCurrent.removeClass("selected");
		$next.addClass("selected");
		scrollUl($next);
		callback($next);
	}
}
function prevLi($current, moveBy, callback){
	var $origCurrent = $current;
	for(var i=0;i<moveBy;i++){
		var $prev = $current.prev("li");
		if($prev.length){
			$current = $prev;
		}
	}
	if($prev.length){
		$origCurrent.removeClass("selected");
		$prev.addClass("selected");
		scrollUl($prev);
		callback($prev);
	}
}
function scrollUl($selected){
	var position = $selected.position();
	var $ul = $selected.closest("ul");
	var $frame = $ul.closest("div");
	var diff = $frame.width() - position.top + parseInt($ul.first().css("margin-top"));
	if(diff < 0){
		$ul.css("margin-top", diff);
	}else{
		$ul.css("margin-top", 0);
	}
}
//Devices
function switchToConfigRoomsDevicesLeft(){
	configDevicesContext = "left";
}
function insertDevAbc(){
	$target = $("#sett_room_config_bottom_devices_right_abc > ul");
	var first = true;
	for(var i =97;i<=122;i++){
		var char = String.fromCharCode(i);
		$target.append('<li char="' + char + '" ' + (first ? ' class="selected" ': '')+'>' + char + '</li>');
		first = false;
	}
	for(var i =48;i<=57;i++){
		var char = String.fromCharCode(i);
		$target.append('<li char="' + char + '" ' + (first ? ' class="selected" ': '')+'>' + char + '</li>');
		first = false;
	}
	$target.append('<li char="-">-</li>');
	$target.append('<li char=" ">space</li>');
	$target.append('<li char="shift">shift</li>');
	$target.append('<li char="backspace">back<br>space</li>');
}
function insertDevHexAbc(){
	$target = $("#sett_room_config_bottom_devices_right_hexabc > ul");
	var first = true;
	for(var i =48;i<=57;i++){
		var char = String.fromCharCode(i);
		$target.append('<li char="' + char + '" ' + (first ? ' class="selected" ': '')+'>' + char + '</li>');
		first = false;
	}
	for(var i =97;i<=102;i++){
		var char = String.fromCharCode(i);
		$target.append('<li char="' + char + '" ' + (first ? ' class="selected" ': '')+'>' + char + '</li>');
		first = false;
	}
	
	$target.append('<li char="backspace">back<br>space</li>');
}
function switchConfigDevicesToLeft(){
	configDevicesContext = "left";
	var $label = $("#sett_room_config_bottom_devices_name");
	$label.addClass("selected");
}
function switchConfigDevicesToRight(){
	configDevicesContext = "devices";
	var $selected = $("#sett_room_config_bottom_devices_left").find(".selected");
	$selected.removeClass("selected");
	var $selectedRight = $("#sett_room_config_bottom_devices_right_devices").find(".selected");
	configRoomsDevicesShowDevice($selectedRight);
}
function showConfigDevicesAbc($target){
	configDevicesContext = "abc";
	var $abc = $("#sett_room_config_bottom_devices_right_abc");
	var $devices = $("#sett_room_config_bottom_devices_right_devices");
	$devices.hide();
	$abc.show();
}
function showConfigDevicesHexAbc($target){
	configDevicesContext = "hexabc";
	var $abc = $("#sett_room_config_bottom_devices_right_hexabc");
	var $devices = $("#sett_room_config_bottom_devices_right_devices");
	$devices.hide();
	$abc.show();
}
function hideConfigDevicesAbc(){
	configDevicesContext = "left";
	var $abc = $("#sett_room_config_bottom_devices_right_abc");
	var $devices = $("#sett_room_config_bottom_devices_right_devices");
	$abc.hide();
	$devices.show();
}
function hideConfigDevicesHexAbc(){
	configDevicesContext = "left";
	var $abc = $("#sett_room_config_bottom_devices_right_hexabc");
	var $devices = $("#sett_room_config_bottom_devices_right_devices");
	$abc.hide();
	$devices.show();
}
function showConfigDevicesActorNames(){
	configDevicesContext = "actor names";
	var $actors = $("#sett_room_config_bottom_devices_right_prod_types");
	var $devices = $("#sett_room_config_bottom_devices_right_devices");
	$actors.show();
	$devices.hide();
}
function hideConfigDevicesActorNames(){
	configDevicesContext = "left";
	var $actors = $("#sett_room_config_bottom_devices_right_prod_types");
	var $devices = $("#sett_room_config_bottom_devices_right_devices");
	$actors.hide();
	$devices.show();
}
function showConfigDevicesDeviceTypes(){
	configDevicesContext = "dev types";
	var $types = $("#sett_room_config_bottom_devices_right_dev_types");
	var $devices = $("#sett_room_config_bottom_devices_right_devices");
	$types.show();
	$devices.hide();
}
function hideConfigDevicesDeviceTypes(){
	configDevicesContext = "left";
	var $types = $("#sett_room_config_bottom_devices_right_dev_types");
	var $devices = $("#sett_room_config_bottom_devices_right_devices");
	$types.hide();
	$devices.show();
}
function configDevicesDownEdit(){
	var $left = $("#sett_room_config_bottom_devices_left");
	var $selected = $left.find(".selected");
	if($selected.attr("id") == "sett_room_config_bottom_devices_name"){
		$selected.removeClass("selected");
		$left.find("#sett_room_config_bottom_devices_actor_addr").addClass("selected");
	}
}
function configDevicesUpEdit(){
	var $left = $("#sett_room_config_bottom_devices_left");
	var $selected = $left.find(".selected");
	var selectedId = $selected.attr("id");
	if(selectedId == "sett_room_config_bottom_devices_actor_addr" || selectedId == "sett_room_config_bottom_devices_actor_mame" ||
			selectedId == "sett_room_config_bottom_devices_device_type" )
	{
		$selected.removeClass("selected");
		$left.find("#sett_room_config_bottom_devices_name").addClass("selected");
	}
}
function configDevicesPrevEdit(){
	var $left = $("#sett_room_config_bottom_devices_left");
	var $selected = $left.find(".selected");
	var selectedId = $selected.attr("id");
	if( selectedId == "sett_room_config_bottom_devices_actor_mame"){
		$selected.removeClass("selected");
		$left.find("#sett_room_config_bottom_devices_actor_addr").addClass("selected");
	}else if(selectedId == "sett_room_config_bottom_devices_device_type" ){
		$selected.removeClass("selected");
		$left.find("#sett_room_config_bottom_devices_actor_mame").addClass("selected");
	}
	
}
function configDevicesNextEdit(){
	var $left = $("#sett_room_config_bottom_devices_left");
	var $selected = $left.find(".selected");
	var selectedId = $selected.attr("id");
	if( selectedId == "sett_room_config_bottom_devices_actor_addr"){
		$selected.removeClass("selected");
		$left.find("#sett_room_config_bottom_devices_actor_mame").addClass("selected");
	}else if(selectedId == "sett_room_config_bottom_devices_actor_mame" ){
		$selected.removeClass("selected");
		$left.find("#sett_room_config_bottom_devices_device_type").addClass("selected");
	}
}
//Abc
function configDevicesAbcDown(){
	nextLi($("#sett_room_config_bottom_devices_right_abc").find(".selected"), 10, function(){});
}
function configDevicesAbcUp(){
	prevLi($("#sett_room_config_bottom_devices_right_abc").find(".selected"), 10, function(){});
}
function configDevicesAbcPrev(){
	prevLi($("#sett_room_config_bottom_devices_right_abc").find(".selected"), 1, function(){});
}
function configDevicesAbcNext(){
	nextLi($("#sett_room_config_bottom_devices_right_abc").find(".selected"), 1, function(){});
}
function configDevicesHexAbcDown(){
	nextLi($("#sett_room_config_bottom_devices_right_hexabc").find(".selected"), 10, function(){});
}
function configDevicesHexAbcUp(){
	prevLi($("#sett_room_config_bottom_devices_right_hexabc").find(".selected"), 10, function(){});
}
function configDevicesHexAbcPrev(){
	prevLi($("#sett_room_config_bottom_devices_right_hexabc").find(".selected"), 1, function(){});
}
function configDevicesHexAbcNext(){
	nextLi($("#sett_room_config_bottom_devices_right_hexabc").find(".selected"), 1, function(){});
}
function configDevicesAbcEnter(){
	var $abc = $("#sett_room_config_bottom_devices_right_abc");
	var $selected = $abc.find(".selected");
	var $left = $("#sett_room_config_bottom_devices_left");
	var $selectedLeft = $left.find(".selected");
	var $selectedLeftSpan = $selectedLeft.find("span");
	var letter = $selected.attr("char");
	if(letter == "backspace"){
		$selectedLeftSpan.html($selectedLeftSpan.text().substring(0, $selectedLeftSpan.text().length - 1));
	}else if(letter == "shift"){
		$abc.find("li").each(function(){
			var $this = $(this);
			var currLetter = $this.attr("char");
			if(currLetter.length == 1){
				var charCode = currLetter.charCodeAt(0);
				if(charCode >= 97 && charCode <= 122){
					$this.html(String.fromCharCode(charCode - 32));
					$this.attr("char", String.fromCharCode(charCode - 32));
				}else if(charCode >= 65 && charCode <= 90){
					$this.html(String.fromCharCode(charCode + 32));
					$this.attr("char", String.fromCharCode(charCode + 32));
				}
			}
		});
	}else{
		$selectedLeftSpan.html($selectedLeftSpan.html() + letter);
	}
}
function configDevicesHexAbcEnter(){
	var $abc = $("#sett_room_config_bottom_devices_right_hexabc");
	var $selected = $abc.find(".selected");
	var $left = $("#sett_room_config_bottom_devices_left");
	var $selectedLeft = $left.find(".selected");
	var $selectedLeftSpan = $selectedLeft.find("span");
	var letter = $selected.attr("char");
	if(letter == "backspace"){
		$selectedLeftSpan.html($selectedLeftSpan.text().substring(0, $selectedLeftSpan.text().length - 1));
	}else{
		$selectedLeftSpan.html($selectedLeftSpan.html() + letter);
	}
}
//Actor
function configDevicesPrevActor(){
	prevLi($("#sett_room_config_bottom_devices_right_prod_types").find(".selected"), 1, function(){});
}
function configDevicesNextActor(){
	nextLi($("#sett_room_config_bottom_devices_right_prod_types").find(".selected"), 1, function(){});
} 
function configDevicesUpActor(){
	prevLi($("#sett_room_config_bottom_devices_right_prod_types").find(".selected"), 2, function(){});
} 
function configDevicesDownActor(){
	nextLi($("#sett_room_config_bottom_devices_right_prod_types").find(".selected"), 2, function(){});
} 
function configDevicesEnterActor(){
	var $left = $("#sett_room_config_bottom_devices_left");
	var $selected = $left.find(".selected");
	
	var $right = $("#sett_room_config_bottom_devices_right_prod_types");
	var $rightSelected = $right.find(".selected");
	$selected.find("span").text($rightSelected.text());
} 

//Device type
function configDevicesPrevType(){
	prevLi($("#sett_room_config_bottom_devices_right_dev_types").find(".selected"), 1, function(){});
}
function configDevicesNextType(){
	nextLi($("#sett_room_config_bottom_devices_right_dev_types").find(".selected"), 1, function(){});
} 
function configDevicesUpType(){
	prevLi($("#sett_room_config_bottom_devices_right_dev_types").find(".selected"), 2, function(){});
} 
function configDevicesDownType(){
	nextLi($("#sett_room_config_bottom_devices_right_dev_types").find(".selected"), 2, function(){});
}
function configDevicesEnterType(){
	var $left = $("#sett_room_config_bottom_devices_left");
	var $selected = $left.find(".selected");
	var $img = $left.find("#sett_room_config_bottom_devices_image");
	
	var $right = $("#sett_room_config_bottom_devices_right_dev_types");
	var $rightSelected = $right.find(".selected");
	$selected.find("span").text($rightSelected.text());
	$img.attr("dev_type", $rightSelected.text());
}
// config devices actions
function configDevicesSave(){
	var $label = $("#sett_room_config_bottom_devices_name > span");
	var $addr = $("#sett_room_config_bottom_devices_actor_addr > span");
	var $actor = $("#sett_room_config_bottom_devices_actor_mame > span");
	var $type = $("#sett_room_config_bottom_devices_device_type > span");
	var obj = new Object();
	if(!configDevicesIsNew){
		var $selected = $("#sett_room_config_bottom_devices_right_devices").find(".selected");
		obj['id'] = $selected.attr("dev_id");
	}
	obj['device info'] = new Object();
	obj['device info']['label'] = $label.text();
	obj['device info']['address'] = parseInt($addr.text(), 16);
	obj['device info']['product type'] = $actor.text();
	obj['device info']['type'] = $type.text();
	var data_json = JSON.stringify(obj);
	alert(data_json);
	var sendType = "POST";
	var url = dev_addr;
	$.ajax({  
		  url:url,  
		  type: sendType,
		  dataType: "json",  
		  contentType: "json",  
		  data: data_json,  
		  success: function(data){  
			  configDevicesReload();
			  loadConfigRooms();
		  },  
		  error: function(data){  
			  //console.log(data);
		  }  
	});
	
}
function configDevicesNew(){
	var $label = $("#sett_room_config_bottom_devices_name > span");
	var $labelMain = $("#sett_room_config_bottom_devices_name");
	var $addr = $("#sett_room_config_bottom_devices_actor_addr > span");
	var $actor = $("#sett_room_config_bottom_devices_actor_mame > span");
	var $type = $("#sett_room_config_bottom_devices_device_type > span");
	var $img = $("#sett_room_config_bottom_devices_image");
	$label.text("");
	$addr.text("");
	$actor.text("");
	$type.text("");
	$img.attr("dev_type", "");
	$labelMain.addClass("selected");
	configDevicesContext = "left";
	configDevicesIsNew = true;
}
function configDevicesDelete(){
	var $selected = $("#sett_room_config_bottom_devices_right_devices").find(".selected");
	var dev_id = $selected.attr("dev_id");
	var url = dev_addr + dev_id;
	//console.log(url);
	$.ajax({  
		  url:url,  
		  type: "DELETE",
		  dataType: "json",  
		  contentType: "json",    
		  success: function(data){  
			  configDevicesReload();
			  
		  },  
		  error: function(data){  
			  //console.log(data);
		  }  
	});
}
function configDevicesReload(){
	loadConfigRoomsDevices();
	switchToConfigRoomsDevices();
	var $left = $("#sett_room_config_bottom_devices_left");
	var $selected = $left.find(".selected");
	$selected.removeClass("selected");
	var $right = $("#sett_room_config_bottom_devices_right_devices > ul");
	$right.find("li").first().addClass("selected");
	$right.css("margin-top", "0px");
}
///////////////////
//Floorplans config
//

function loadConfigFloorplans(){
	var first = true;
	var $floorplansLeft = $("#sett_room_config_bottom_floorplan_left > ul");
	$floorplansLeft.html("");
	var $floorplansRight = $("#sett_room_config_bottom_floorplan_right");
	$floorplansRight.html("");
	var $floorplansRooms = $("#sett_room_config_bottom_room_floorplans >ul");
	$floorplansRooms.html("");
	var $floorplansRoomsRight = $("#sett_room_config_bottom_room_right_floorplan");
	$floorplansRoomsRight.html("");
	var url = apiAddr + "/floorplans/";
	$.getJSON(url, function(data) {
		$.each(data, function(key, val) {	
			$floorplansLeft.append("<li floorplan='" + key + "' class='" + (first ? "selected":"") + "' >" + key + "</li>");
			$floorplansRight.append('<div floorplan="' + key + '" class="config_floorplan" style="background-image:url(\'' + val + '\');' + (first ? '' : 'display:none;') + '"></div>');
			$floorplansRooms.append("<li floorplan='" + key + "' class='" + (first ? "selected":"") + "' >" + key + "</li>");
			$floorplansRoomsRight.append('<div floorplan="' + key + '" class="config_rooms_floorplan" style="background-image:url(\'' + val + '\');display:none;"></div>');
			first = false;
		});
	});
}
function configFloorplanPrev(){
	prevLi($("#sett_room_config_bottom_floorplan_left .selected"), 1, configFloorplanShow);
}
function configFloorplanNext(){
	nextLi($("#sett_room_config_bottom_floorplan_left .selected"), 1, configFloorplanShow);
}
function configFloorplanUp(){
	prevLi($("#sett_room_config_bottom_floorplan_left .selected"), 2, configFloorplanShow);
}
function configFloorplanDown(){
	nextLi($("#sett_room_config_bottom_floorplan_left .selected"), 2, configFloorplanShow);
}
function configFloorplanShow(){
	var $left = $("#sett_room_config_bottom_floorplan_left");
	var $right = $("#sett_room_config_bottom_floorplan_right");
	var $selected = $left.find(".selected");
	var floorplan = $selected.attr("floorplan");
	$right.find("[floorplan]").hide();
	$right.find("[floorplan='" + floorplan + "']").show();
}
function switchToConfigRoomsFloorplan(){
	configMainContext = "floorplan";
}
function configFloorplanDelete(){
	var $left = $("#sett_room_config_bottom_floorplan_left");
	var $selected = $left.find(".selected");
	
	var url = apiAddr+ "/configuration/floorplans/" + $selected.attr("floorplan")+"/";
	$.ajax({  
		  url:url,  
		  type: "DELETE",
		  dataType: "json",  
		  contentType: "json",    
		  success: function(data){  
			  configFloorplansReload();
		  },  
		  error: function(data){  
			  
		  }  
	});
}
function configFloorplansReload(){
	loadConfigFloorplans();
	configFloorplanShow();
}
///////
//Rooms
///////
function loadConfigRooms(checkFirst){
	if(typeof checkFirst == "undefined"){
		checkFirst = false;
	}
	roomsDevices = new Object();
	var first = true;
	var $rooms = $("#sett_room_config_bottom_room_rooms > ul");
	$rooms.html("");
	var $settRooms = $("#sett_room_config_bottom_pair_left_rooms > ul");
	$settRooms.html("");
	var $settDev = $("#sett_room_config_bottom_pair_left_devices");
	$settDev.html("");
	var $settFloorplans = $("#sett_room_config_bottom_pair_right_floorplan");
	$settFloorplans.html("");
	$rooms.css("margin-top", "0px");
	var url = apiAddr + "/rooms/";
	$.getJSON(url, function(data) {
		$.each(data, function(key, val) {
			$.getJSON(val.url, function(data_room) {
				var floorplan = "";
				if(typeof data_room.floorplan === 'string'){
					floorplan = data_room.floorplan;
				}else if(typeof data_room.floorplan === 'object'){
					if(data_room.floorplan == null){
						floorplan = "";
					}else{
						floorplan = data_room.floorplan.image;
					}
				}
				$rooms.append("<li room='" + data_room.id + "' label='" + data_room['room info'].label + "' floorplan='" + floorplan + "' class='" + (first ? "selected":"") + "' >" + data_room['room info'].label + "</li>");
				loadPairRooms(data_room, first, floorplan, checkFirst);
				roomsDevices[data_room.id] = data_room.devices;
				$.each(data_room.devices, function(key_dev, val_dev){
					
				});
				if(first){
					showConfigRoomsSelected();
				}
				first = false;
			});
			
		});
	});
	
}
function switchToConfigRoomsRooms(){
	configMainContext = "rooms";
}
function showConfigRoomsSelected(){
	var $left = $("#sett_room_config_bottom_room_left");
	var $right = $("#sett_room_config_bottom_room_right");
	var $rooms = $left.find("#sett_room_config_bottom_room_rooms > ul");
	var $selected = $rooms.find(".selected");
	var $name = $left.find("#sett_room_config_bottom_room_name > span");
	var $floorplan = $left.find("#sett_room_config_bottom_room_floorplan > span");
	var $floorplansRight = $right.find("[floorplan]");
	$name.html($selected.attr("label"));
	$floorplan.html($selected.attr("floorplan"));
	$floorplansRight.hide();
	$right.find("[floorplan='" + $selected.attr("floorplan") + "']").show();
}
function configRoomsPrev(){
	prevLi($("#sett_room_config_bottom_room_rooms").find(".selected"), 1, showConfigRoomsSelected);
}
function configRoomsNext(){
	nextLi($("#sett_room_config_bottom_room_rooms").find(".selected"), 1, showConfigRoomsSelected);
} 
function configRoomsUp(){
	prevLi($("#sett_room_config_bottom_room_rooms").find(".selected"), 2, showConfigRoomsSelected);
} 
function configRoomsDown(){
	nextLi($("#sett_room_config_bottom_room_rooms").find(".selected"), 2, showConfigRoomsSelected);
}
function configRoomsEnter(){
	configRoomsContext = "top";
	var $rooms = $("#sett_room_config_bottom_room");
	var $label = $rooms.find("#sett_room_config_bottom_room_name");
	$label.addClass("selected");
}
function configRoomsTopLeave(){
	configRoomsContext = "rooms";
	var $label = $("#sett_room_config_bottom_room_name");
	var $floorplan = $("#sett_room_config_bottom_room_floorplan");
	$label.removeClass("selected");
	$floorplan.removeClass("selected");
	configRoomIsNew = false;
}
function insertRoomsAbc(){
	var $target = $("#sett_room_config_bottom_room_abc > ul");
	var first = true;
	for(var i =97;i<=122;i++){
		var char = String.fromCharCode(i);
		$target.append('<li char="' + char + '" ' + (first ? ' class="selected" ': '')+'>' + char + '</li>');
		first = false;
	}
	for(var i =48;i<=57;i++){
		var char = String.fromCharCode(i);
		$target.append('<li char="' + char + '" ' + (first ? ' class="selected" ': '')+'>' + char + '</li>');
		first = false;
	}
	$target.append('<li char="-">-</li>');
	$target.append('<li char=" ">space</li>');
	$target.append('<li char="shift">shift</li>');
	$target.append('<li char="backspace">back<br>space</li>');
}
function showConfigRoomsAbc(){
	configRoomsContext = "abc";
	var $abc = $("#sett_room_config_bottom_room_abc");
	var $devices = $("#sett_room_config_bottom_room_rooms");
	$devices.hide();
	$abc.show();
}
function hideConfigRoomsAbc(){
	configRoomsContext = "top";
	var $abc = $("#sett_room_config_bottom_room_abc");
	var $devices = $("#sett_room_config_bottom_room_rooms");
	$abc.hide();
	$devices.show();
}
function configRoomsTopDown(){
	var $name = $("#sett_room_config_bottom_room_name");
	var $floorplan = $("#sett_room_config_bottom_room_floorplan");
	if($name.hasClass("selected")){
		$name.removeClass("selected");
		$floorplan.addClass("selected");
	}
}
function configRoomsTopUp(){
	var $name = $("#sett_room_config_bottom_room_name");
	var $floorplan = $("#sett_room_config_bottom_room_floorplan");
	if($floorplan.hasClass("selected")){
		$floorplan.removeClass("selected");
		$name.addClass("selected");
	}
}
function configRoomsTopEnter(){
	var $name = $("#sett_room_config_bottom_room_name");
	var $floorplan = $("#sett_room_config_bottom_room_floorplan");
	if($name.hasClass("selected")){
		showConfigRoomsAbc();
	}else if($floorplan.hasClass("selected")){
		showConfigRoomsFloorplan();
	}
}
function configRoomsAbcPrev(){
	prevLi($("#sett_room_config_bottom_room_abc").find(".selected"), 1, function(){});
}
function configRoomsAbcNext(){
	nextLi($("#sett_room_config_bottom_room_abc").find(".selected"), 1, function(){});
} 
function configRoomsAbcUp(){
	prevLi($("#sett_room_config_bottom_room_abc").find(".selected"), 10, function(){});
} 
function configRoomsAbcDown(){
	nextLi($("#sett_room_config_bottom_room_abc").find(".selected"), 10, function(){});
}
function configRoomsAbcEnter(){
	var $abc = $("#sett_room_config_bottom_room_abc");
	var $selected = $abc.find(".selected");
	var $selectedLeft = $("#sett_room_config_bottom_room_name")
	var $selectedLeftSpan = $selectedLeft.find("span");
	var letter = $selected.attr("char");
	if(letter == "backspace"){
		$selectedLeftSpan.html($selectedLeftSpan.text().substring(0, $selectedLeftSpan.text().length - 1));
	}else if(letter == "shift"){
		$abc.find("li").each(function(){
			var $this = $(this);
			var currLetter = $this.attr("char");
			if(currLetter.length == 1){
				var charCode = currLetter.charCodeAt(0);
				if(charCode >= 97 && charCode <= 122){
					$this.html(String.fromCharCode(charCode - 32));
					$this.attr("char", String.fromCharCode(charCode - 32));
				}else if(charCode >= 65 && charCode <= 90){
					$this.html(String.fromCharCode(charCode + 32));
					$this.attr("char", String.fromCharCode(charCode + 32));
				}
			}
		});
	}else{
		$selectedLeftSpan.html($selectedLeftSpan.html() + letter);
	}
}
function showConfigRoomsFloorplan(){
	configRoomsContext = "floorplans";
	var $floorplan = $("#sett_room_config_bottom_room_floorplans");
	var $rooms = $("#sett_room_config_bottom_room_rooms");
	var floorplan = $("#sett_room_config_bottom_room_floorplan").find("span").text();
	var $floorplans = $floorplan.find("[floorplan]");
	$floorplans.removeClass("selected");
	var $selectedFloorplan = $floorplan.find("[floorplan='" + floorplan + "']");
	if($selectedFloorplan.length){
		$selectedFloorplan.addClass("selected");
	}else{
		$floorplans.first().addClass("selected");
	}
	$rooms.hide();
	$floorplan.show();
	
	 configRoomsFloorplansChange();
}
function hideConfigRoomsFloorplan(){
	configRoomsContext = "top";
	var $floorplan = $("#sett_room_config_bottom_room_floorplans");
	var $rooms = $("#sett_room_config_bottom_room_rooms");
	$rooms.show();
	$floorplan.hide();
}
function cancelConfigRoomsFloorplan(){
	configRoomsContext = "top";
	var $rooms = $("#sett_room_config_bottom_room_rooms");
	var $right = $("#sett_room_config_bottom_room_right");
	var $selected = $rooms.find(".selected");
	var $floorplansRight = $right.find("[floorplan]");
	$floorplansRight.hide();
	$right.find("[floorplan='" + $selected.attr("floorplan") + "']").show();
}
function approveConfigRoomsFloorplan(){
	configRoomsContext = "top";
	var $right = $("#sett_room_config_bottom_room_right");
	var $floorplanEdit = $("#sett_room_config_bottom_room_floorplan").find("span");
	var $floorplanSelected = $("#sett_room_config_bottom_room_floorplans").find(".selected");
	var floorplan = $floorplanSelected.attr("floorplan");
	$floorplanEdit.html(floorplan);
	var $floorplansRight = $right.find("[floorplan]");
	$floorplansRight.hide();
	$right.find("[floorplan='" + floorplan + "']").show();
}
function configRoomsFloorplansPrev(){
	prevLi($("#sett_room_config_bottom_room_floorplans").find(".selected"), 1, configRoomsFloorplansChange);
}
function configRoomsFloorplansNext(){
	nextLi($("#sett_room_config_bottom_room_floorplans").find(".selected"), 1, configRoomsFloorplansChange);
}
function configRoomsFloorplansUp(){
	prevLi($("#sett_room_config_bottom_room_floorplans").find(".selected"), 2, configRoomsFloorplansChange);
}
function configRoomsFloorplansDown(){
	nextLi($("#sett_room_config_bottom_room_floorplans").find(".selected"), 2, configRoomsFloorplansChange);
}
function configRoomsFloorplansChange(){
	var $selected = $("#sett_room_config_bottom_room_floorplans").find(".selected");
	var floorplan = $selected.attr("floorplan");
	var $right = $("#sett_room_config_bottom_room_right");
	var $floorplansRight = $right.find("[floorplan]");
	$floorplansRight.hide();
	$right.find("[floorplan='" + floorplan + "']").show();
}
function configRoomsSave(){
	var $label = $("#sett_room_config_bottom_room_name > span");
	var $floorplan = $("#sett_room_config_bottom_room_floorplan > span");
	var floorplan = $floorplan.text();
	var obj = new Object();
	var $selectedRoom = $("#sett_room_config_bottom_room_rooms").find(".selected");
	if(!configRoomIsNew && $selectedRoom.attr("room") != undefined){
		obj['id'] = $selectedRoom.attr("room");
		obj['devices'] = roomsDevices[$selectedRoom.attr("room")];
	}else{
		obj['devices'] = new Object();
	}
	obj['room info'] = new Object();
	obj['room info']['label'] = $label.text();
	if(floorplan === ""){
		obj['floorplan'] = null;		
	}else{
		obj['floorplan'] = new Object();
		obj['floorplan']['image'] = floorplan;	
	}
	alert(JSON.stringify(obj));
	var data_json = JSON.stringify(obj);
	var sendType = "POST";
	var url = apiAddr + "/rooms";
	$.ajax({  
		  url:url,  
		  type: sendType,
		  dataType: "json",  
		  contentType: "json",  
		  data: data_json,  
		  success: function(data){  
			  configRoomsReload();
		  },  
		  error: function(data){  
			 
		  }  
	});
}
function configRoomsNew(){
	configRoomIsNew = true;
	var $floorplan = $("#sett_room_config_bottom_room_floorplan > span");
	var $name = $("#sett_room_config_bottom_room_name > span");
	$floorplan.html("");
	$name.html("");
	configRoomsEnter();
	showConfigRoomsAbc();
}
function configRoomsDelete(){
	var $selectedRoom = $("#sett_room_config_bottom_room_rooms").find(".selected");
	var sendType = "DELETE";
	var url = apiAddr + "/rooms/" + $selectedRoom.attr("room");
	$.ajax({  
		  url:url,  
		  type: sendType,
		  dataType: "json",  
		  contentType: "json",    
		  success: function(data){  
			  configRoomsReload();
		  },  
		  error: function(data){  
			  configRoomsReload();
		  }  
	});
}
function configRoomsReload(checkFirst){
	if(typeof checkFirst == "undefined"){
		checkFirst = false;
	}
	loadConfigRooms(checkFirst);
	configRoomsTopLeave();
}
//////////
//Pairing
function loadPairRooms(data_room, first, floorplan, checkFirst){
	if(typeof checkFirst == "undefined"){
		checkFirst = false;
	}
	var $settRooms = $("#sett_room_config_bottom_pair_left_rooms > ul");
	var $settDev = $("#sett_room_config_bottom_pair_left_devices");
	var $settFloorplans = $("#sett_room_config_bottom_pair_right_floorplan");
	
	$settRooms.append("<li room='" + data_room.id + "' label='" + data_room['room info'].label + "' floorplan='" + floorplan + "'  class='" + ((checkFirst && first)?'selected':'') + "'>" + data_room['room info'].label + "</li>");
	$settDev.append("<div style='" + (first ? '' : 'display:none;') + "' room='" + data_room.id + "'><ul></ul></div>");
	var $roomDevs = $settDev.find("[room='" + data_room.id + "']");
	if(floorplan != ""){
		var floorplanCss = (floorplan != '' ? 'background-image:url("' + floorplanAddr+floorplan + '");' : '');
		$settFloorplans.append("<div class='sett_room_floorplan' style='" + (first ? '' : 'display:none;') + floorplanCss +"' room='" + data_room.id + "'><ul></ul></div>");
	}
	var $floorplanDevs = $settFloorplans.find("[room='" + data_room.id + "']");
	
	$.getJSON(dev_addr, function(data) {
		$.each(data, function(key, val) {
			if(key!= ""){
				$.getJSON(val.url, function(data_dev) {
					var inRoom = false;
					if(typeof data_room.devices[data_dev.id] != 'undefined'){
						inRoom = true;
					}
					alert(data_room.id + ":"+data_dev.id+":" + inRoom);
					var coordinateX = 0;
					var coordinateY = 0;
					if(inRoom && data_room.devices[data_dev.id].coordinates != null){
						coordinateX = data_room.devices[data_dev.id].coordinates[0];
						coordinateY = data_room.devices[data_dev.id].coordinates[1];
					}
					var appendText = "<li dev_id='" + data_dev.id + "' label='" + data_dev['device info'].label + "' dev_type='" + data_dev['device info'].type + "'  dev_addr='" + data_dev['device info'].address + "' ><div class='sett_dev_label'>" + data_dev['device info'].label + "</div><div class='sett_dev_check' " +(inRoom ? 'checked="1" ': '') + "></div></li>";
					if(inRoom){
						$roomDevs.find("ul").prepend(appendText);
					}else{
						$roomDevs.find("ul").append(appendText);
					}
					$floorplanDevs.find("ul").append("<div style='" +(inRoom ? '': 'display:none;') + "' class='sett_dev_image' dev_id='" + data_dev.id + "' label='" + data_dev['device info'].label + "' dev_type='" + data_dev['device info'].type + "' coordinateX='" + coordinateX + "' coordinateY='" + coordinateY + "'></div>");
					var $devOnFloorplan = $floorplanDevs.find("[dev_id='" + data_dev.id + "']");
					configFloorplanSetCoordinates($devOnFloorplan);
				});
			}
		});
	});
}
function switchToConfigRoomsPair(){
	configMainContext = "pair";
	configPairContext = "rooms";
	var $rooms = $("#sett_room_config_bottom_pair_left_rooms");
	var $last = $rooms.find(".lastSelected");
	if($last.length){
		$last.removeClass("lastSelected").addClass("selected");
	}else{
		$rooms.find("li").first().addClass("selected");
	}
}
function configPairRoomUp(){
	var $rooms = $("#sett_room_config_bottom_pair_left_rooms");
	var $selected = $rooms.find(".selected");
	prevLi($selected, 1, configPairRoomShowSelected);
}
function configPairRoomDown(){
	var $rooms = $("#sett_room_config_bottom_pair_left_rooms");
	var $selected = $rooms.find(".selected");
	nextLi($selected, 1, configPairRoomShowSelected);
}
function configPairRoomShowSelected(){
	var $rooms = $("#sett_room_config_bottom_pair_left_rooms");
	var $selected = $rooms.find(".selected");
	var $devicesRooms = $("#sett_room_config_bottom_pair_left_devices");
	var $floorplansRooms = $("#sett_room_config_bottom_pair_right_floorplan");
	$devicesRooms.find("[room]").hide();
	$devicesRooms.find("[room='" + $selected.attr("room") + "']").show();
	$floorplansRooms.find("[room]").hide();
	$floorplansRooms.find("[room='" + $selected.attr("room") + "']").show();
}
function configPairDeviceUp(){
	var $rooms = $("#sett_room_config_bottom_pair_left_rooms");
	var $selected = $rooms.find(".selected");
	var $devices = $("#sett_room_config_bottom_pair_left_devices");
	var $currDevices = $devices.find("[room='" + $selected.attr("room") + "']");
	var $selectedDev = $currDevices.find(".selected");
	prevLi($selectedDev, 1, configPairDevShowSelected);
}
function configPairDeviceDown(){
	var $rooms = $("#sett_room_config_bottom_pair_left_rooms");
	var $selected = $rooms.find(".selected");
	var $devices = $("#sett_room_config_bottom_pair_left_devices");
	var $currDevices = $devices.find("[room='" + $selected.attr("room") + "']");
	var $selectedDev = $currDevices.find(".selected");
	nextLi($selectedDev, 1, configPairDevShowSelected);
}
function configPairDevShowSelected(){
	
}
function switchConfigPairToDevices(){
	configPairContext = "devices";
	var $rooms = $("#sett_room_config_bottom_pair_left_rooms");
	var $selected = $rooms.find(".selected");
	var $devices = $("#sett_room_config_bottom_pair_left_devices");
	var $currDevices = $devices.find("[room='" + $selected.attr("room") + "']");
	var $last = $currDevices.find(".lastSelected");
	if($last.length){
		$last.removeClass("lastSelected").addClass("selected");
	}else{
		$currDevices.find("li").first().addClass("selected");
	}
	
}
function switchConfigPairToRooms(){
	configPairContext = "rooms";
	var $rooms = $("#sett_room_config_bottom_pair_left_rooms");
	var $selected = $rooms.find(".selected");
	var $devices = $("#sett_room_config_bottom_pair_left_devices");
	var $currDevices = $devices.find("[room='" + $selected.attr("room") + "']");
	$currDevices.find(".selected").removeClass("selected").addClass("lastSelected");
}
function configFloorplanSetCoordinates($el){
	configFloorplanSetCoordinatesPair($el, $el.attr("coordinatex"), $el.attr("coordinatey"));
}
function configFloorplanSetTempCoordinates($el){
	configFloorplanSetCoordinatesPair($el, $el.attr("tempx"), $el.attr("tempy"));
}
function configFloorplanSetCoordinatesPair($el, x, y){
	var left = Math.floor(x * max_width);
	var top = Math.floor(y * max_height);
	if(left >= (max_width-10)){
		left = max_width-11;
	}
	if(left < 3){
		left += 0;  
	}
	if(top >= (max_height-10)){
		top = max_height-11;
	}
	if(top < 3){
		top += 0;
	}
	$el.css('left', left + 'px');
	$el.css('top', top + 'px');	
}

function switchConfigPairDevice(){
	var $rooms = $("#sett_room_config_bottom_pair_left_rooms");
	var $selected = $rooms.find(".selected");
	var $devices = $("#sett_room_config_bottom_pair_left_devices");
	var $currDevices = $devices.find("[room='" + $selected.attr("room") + "']");
	var $currDevice = $currDevices.find(".selected");
	var $checkbox = $currDevice.find(".sett_dev_check");
	var $floorplan = $("#sett_room_config_bottom_pair_right_floorplan").find("[room='" + $selected.attr("room") + "']");
	var $devOnFloorplan = $floorplan.find("[dev_id='" + $currDevice.attr("dev_id") + "']");
	if($checkbox.attr("checked")){
		$checkbox.removeAttr("checked");
		$devOnFloorplan.hide();
		$devOnFloorplan.css("border", "");
		
	}else{
		$checkbox.attr("checked", "1");
		$devOnFloorplan.show();
		$devOnFloorplan.css("border", "1px solid red");
		if($floorplan.length){
			switchToFloorplanDevice();
		}
	}
}
function switchToFloorplanDevice(){
	configPairContext = "floorplans";
}
function getDevOnFloorplan(){
	var $rooms = $("#sett_room_config_bottom_pair_left_rooms");
	var $selected = $rooms.find(".selected");
	var $devices = $("#sett_room_config_bottom_pair_left_devices");
	var $currDevices = $devices.find("[room='" + $selected.attr("room") + "']");
	var $currDevice = $currDevices.find(".selected");
	var $floorplan = $("#sett_room_config_bottom_pair_right_floorplan").find("[room='" + $selected.attr("room") + "']");
	var $devOnFloorplan = $floorplan.find("[dev_id='" + $currDevice.attr("dev_id") + "']");
	return $devOnFloorplan;
}
function configPairDeviceFloorplanLeft(){
	var $devOnFloorplan = getDevOnFloorplan();
	var x = 0;
	var y = 0;
	if($devOnFloorplan.attr("tempX")){
		x = parseFloat($devOnFloorplan.attr("tempx"));
		y = parseFloat($devOnFloorplan.attr("tempy"));
	}else{
		x = parseFloat($devOnFloorplan.attr("coordinatex"));
		y = parseFloat($devOnFloorplan.attr("coordinatey"));
	}
	if(x>0){
		x -= 0.05;
	}
	$devOnFloorplan.attr("tempx", x);
	$devOnFloorplan.attr("tempy", y);
	configFloorplanSetTempCoordinates($devOnFloorplan);
}
function configPairDeviceFloorplanRight(){
	var $devOnFloorplan = getDevOnFloorplan();
	var x = 0;
	var y = 0;
	if($devOnFloorplan.attr("tempX")){
		x = parseFloat($devOnFloorplan.attr("tempx"));
		y = parseFloat($devOnFloorplan.attr("tempy"));
	}else{
		x = parseFloat($devOnFloorplan.attr("coordinatex"));
		y = parseFloat($devOnFloorplan.attr("coordinatey"));
	}
	if(x<1){
		x += 0.05;
	}
	$devOnFloorplan.attr("tempx", x);
	$devOnFloorplan.attr("tempy", y);
	configFloorplanSetTempCoordinates($devOnFloorplan);
}
function configPairDeviceFloorplanUp(){
	var $devOnFloorplan = getDevOnFloorplan();
	var x = 0;
	var y = 0;
	if($devOnFloorplan.attr("tempX")){
		x = parseFloat($devOnFloorplan.attr("tempx"));
		y = parseFloat($devOnFloorplan.attr("tempy"));
	}else{
		x = parseFloat($devOnFloorplan.attr("coordinatex"));
		y = parseFloat($devOnFloorplan.attr("coordinatey"));
	}
	if(y>0){
		y -= 0.05;
	}
	$devOnFloorplan.attr("tempx", x);
	$devOnFloorplan.attr("tempy", y);
	configFloorplanSetTempCoordinates($devOnFloorplan);
}
function configPairDeviceFloorplanDown(){
	var $devOnFloorplan = getDevOnFloorplan();
	var x = 0;
	var y = 0;
	if($devOnFloorplan.attr("tempX")){
		x = parseFloat($devOnFloorplan.attr("tempx"));
		y = parseFloat($devOnFloorplan.attr("tempy"));
	}else{
		x = parseFloat($devOnFloorplan.attr("coordinatex"));
		y = parseFloat($devOnFloorplan.attr("coordinatey"));
	}
	if(y<1){
		y += 0.05;
	}
	$devOnFloorplan.attr("tempx", x);
	$devOnFloorplan.attr("tempy", y);
	configFloorplanSetTempCoordinates($devOnFloorplan);
}
function configPairDeviceFloorplanSave(){
	var $devOnFloorplan = getDevOnFloorplan();
	$devOnFloorplan.attr("coordinatex", $devOnFloorplan.attr("tempx"));
	$devOnFloorplan.attr("coordinatey", $devOnFloorplan.attr("tempy"));
	$devOnFloorplan.removeAttr("tempx");
	$devOnFloorplan.removeAttr("tempy");
	$devOnFloorplan.css("border", "0");
	configPairContext = "devices";
}
function configPairDeviceFloorplanCancel(){
	var $devOnFloorplan = getDevOnFloorplan();
	$devOnFloorplan.removeAttr("tempx");
	$devOnFloorplan.removeAttr("tempy");
	configFloorplanSetCoordinates($devOnFloorplan);
	$devOnFloorplan.css("border", "0");
	configPairContext = "devices";
}
function configPairDevicePairSave(){
	var $rooms = $("#sett_room_config_bottom_pair_left_rooms");
	var $devices = $("#sett_room_config_bottom_pair_left_devices");
	var $floorplan = $("#sett_room_config_bottom_pair_right_floorplan");
	var lastRoom = $rooms.find("[room]").last().attr("room");
	$rooms.find("[room]").each(function(){
		var $this = $(this);
		var roomObj = new Object();
		var roomId = $this.attr("room");
		roomObj.id = roomId;
		roomObj['room info'] = new Object();
		roomObj['room info'].label = $this.attr("label");
		var floorplan = $this.attr("floorplan");
		var hasFloorplan = false;
		if(floorplan === ""){
			roomObj['floorplan'] = null;
		}else{
			roomObj['floorplan'] = new Object();
			roomObj['floorplan'].image = floorplan;
			hasFloorplan = true;
		}
		var $currDevices = $devices.find("[room='" + roomId + "']");
		var $allDevicesInRoom = $currDevices.find("[dev_id]");
		var $_room = $this;
		roomObj.devices = new Object();
		$allDevicesInRoom.each(function(){
			var $this = $(this);
			var $checkbox = $this.find(".sett_dev_check");
			if($checkbox.attr("checked")){
				var devId = $this.attr("dev_id");
				roomObj.devices[devId] = new Object();
				if(hasFloorplan){
					var $currFloorplan = $floorplan.find("[room='" + roomId + "']");
					var $devOnFloorplan = $currFloorplan.find("[dev_id='" + devId + "']");
					var x = parseFloat($devOnFloorplan.attr("coordinatex"));
					var y = parseFloat($devOnFloorplan.attr("coordinatey"));
					roomObj.devices[devId].coordinates = [x, y];
				}else{
					roomObj.devices[devId].coordinates = null;
				}
			}
		});
		var data_json = JSON.stringify(roomObj);
		var sendType = "POST";
		var url = apiAddr + "/rooms";
		$.ajax({  
			  url:url,  
			  type: sendType,
			  dataType: "json",  
			  contentType: "json",  
			  data: data_json,  
			  success: function(data){  
				  if(lastRoom == roomId){
					  configPairRoomsReload();
				  }
			  },  
			  error: function(data){  
				 
			  }  
		});
		
	});
	
}
function configPairRoomsReload(){
	configRoomsReload(true);
	var $rooms = $("#sett_room_config_bottom_pair_left_rooms");
	var $roomsUl = $rooms.find("ul");
	$roomsUl.children().first().addClass("selected");
	$roomsUl.css("margin-top", "0px");
	configPairContext = "rooms";
}

///////////////////////////
//Cameras
///////////////////////
function loadCams(){
	var strCamIds = getFile("cams");
	alert(strCamIds);
	camIds = JSON.parse(strCamIds);
	if(!camIds){
		camIds = new Array();
	}
	
	$.each(camIds, function(key, val){
		camsAppendCamToList(val);
	});
}
function camsAppendCamToList(val){
	var $camsDevices = $("#sett_room_config_bottom_devices_cams_right_devices");
	var $camsDevicesUl = $camsDevices.find("ul");
	$camsDevicesUl.append("<li name='" + val.name + "' addr='" + val.addr + "'  type='" + val.type + "'  login='" + val.login + "'  pass='" + val.pass + "'>" + val.name + "</li>");
}
function showCamSelected(){
	var $camsDevices = $("#sett_room_config_bottom_devices_cams_right_devices");
	var $selected = $camsDevices.find(".selected");
	
	if($selected.length){
		showCam($selected);
	}
}
function showCam($el){
	var $name = $("#sett_room_config_bottom_devices_cams_name > span");
	var $addr = $("#sett_room_config_bottom_devices_cam_addr > span");
	var $type = $("#sett_room_config_bottom_devices_cam_type > span");
	var $login = $("#sett_room_config_bottom_devices_cams_login > span");
	var $pass = $("#sett_room_config_bottom_devices_cams_pass > span");
	$name.html($el.attr("name"));
	$addr.html($el.attr("addr"));
	$type.html($el.attr("type"));
	$login.html($el.attr("login"));
	$pass.html($el.attr("pass"));
}
function saveCams(){
	alert(JSON.stringify(camIds));
	setFile("cams", JSON.stringify(camIds));
}
function switchToOtherIp(){
	$("#sett_other_config_bottom").find(".sett_room_config_bottom_section").hide();
	$("#ip_setting").show();
	otherContext = "bottom";
	otherMainContext = "ip";
	$("input[type='text']:first").keydown(Main.keydown).focus().select();
}
function prevOther(){
	var $room_conf = $("#sett_other_config");
	var $selected = $room_conf.find(".sett_room_config_top.selected");
	var $next = $selected.prev(".sett_room_config_top");
	if($next.length){
		var bottomId = $next.attr("bottom");
		$room_conf.find(".sett_room_config_top").removeClass("selected");
		$next.addClass("selected");
		$room_conf.find(".sett_room_config_bottom_section").hide();
		$("#" + bottomId).show();
		setupConfigOtherHelp(bottomId);
	}
}
function nextOther(){
	var $room_conf = $("#sett_other_config");
	var $selected = $room_conf.find(".sett_room_config_top.selected");
	var $next = $selected.next(".sett_room_config_top");
	if($next.length){
		var bottomId = $next.attr("bottom");
		$room_conf.find(".sett_room_config_top").removeClass("selected");
		$next.addClass("selected");
		$room_conf.find(".sett_room_config_bottom_section").hide();
		$("#" + bottomId).show();
		alert(bottomId);
		setupConfigOtherHelp(bottomId);
	}
}
function switchToOtherCams(){
	$devices = $("#sett_room_config_bottom_devices_cams_right_devices");
	$devicesUl = $devices.find("ul");
	$lis = $devicesUl.find("li");
	if($lis.length){
		$lis.first().addClass("selected");
		otherContext = "bottom";
		otherMainContext = "devices";
		showCamSelected();
	}else{
		otherContext = "bottom";
		otherMainContext = "left";
		var $name = $("#sett_room_config_bottom_devices_cams_name");
		$name.addClass("selected");
		newCamera = true;
		camsShowAbc();
	}
}
function camsLeftEnter(){
	var $left = $("#sett_room_config_bottom_devices_cams_left");
	var $selected = $left.find(".selected");
	switch($selected.attr("id")){
		case "sett_room_config_bottom_devices_cams_name":
			camsShowAbc();
			break;
		case "sett_room_config_bottom_devices_cam_addr":
			camsShowAbc();
			break;
		case "sett_room_config_bottom_devices_cam_type":
			camsShowType();
			break;
		case "sett_room_config_bottom_devices_cams_login":
			camsShowAbc();
			break;
		case "sett_room_config_bottom_devices_cams_pass":
			camsShowAbc();
			break;
	}
}
function camsLeftUp(){
	var $left = $("#sett_room_config_bottom_devices_cams_left");
	var $selected = $left.find(".selected");
	if($selected.attr("id") == "sett_room_config_bottom_devices_cam_addr" || $selected.attr("id") == "sett_room_config_bottom_devices_cam_type"){
		$selected.removeClass("selected");
		$left.find("#sett_room_config_bottom_devices_cams_name").addClass("selected");
	}else if($selected.attr("id") == "sett_room_config_bottom_devices_cams_login"){
		$selected.removeClass("selected");
		$left.find("#sett_room_config_bottom_devices_cam_addr").addClass("selected");
	}else if($selected.attr("id") == "sett_room_config_bottom_devices_cams_pass"){
		$selected.removeClass("selected");
		$left.find("#sett_room_config_bottom_devices_cams_login").addClass("selected");
	}
}
function camsLeftDown(){
	var $left = $("#sett_room_config_bottom_devices_cams_left");
	var $selected = $left.find(".selected");
	if($selected.attr("id") == "sett_room_config_bottom_devices_cams_name"){
		$selected.removeClass("selected");
		$left.find("#sett_room_config_bottom_devices_cam_addr").addClass("selected");
	}else if($selected.attr("id") == "sett_room_config_bottom_devices_cam_addr" || $selected.attr("id") == "sett_room_config_bottom_devices_cam_type"){
		$selected.removeClass("selected");
		$left.find("#sett_room_config_bottom_devices_cams_login").addClass("selected");
	}else if($selected.attr("id") == "sett_room_config_bottom_devices_cams_login"){
		$selected.removeClass("selected");
		$left.find("#sett_room_config_bottom_devices_cams_pass").addClass("selected");
	}
}
function camsLeftRight(){
	var $left = $("#sett_room_config_bottom_devices_cams_left");
	var $selected = $left.find(".selected");
	if($selected.attr("id") == "sett_room_config_bottom_devices_cam_addr"){
		$selected.removeClass("selected");
		$left.find("#sett_room_config_bottom_devices_cam_type").addClass("selected");
	}
}
function camsLeftLeft(){
	var $left = $("#sett_room_config_bottom_devices_cams_left");
	var $selected = $left.find(".selected");
	if($selected.attr("id") == "sett_room_config_bottom_devices_cam_type"){
		$selected.removeClass("selected");
		$left.find("#sett_room_config_bottom_devices_cam_addr").addClass("selected");
	}
}
function camsShowAbc(){
	var $abc = $("#sett_room_config_bottom_devices_cams_right_abc");
	var $devices = $("#sett_room_config_bottom_devices_cams_right_devices");
	$devices.hide();
	$abc.show();
	otherMainContext = "abc";
}
function camsSwitchFromAbcToLeft(){
	var $abc = $("#sett_room_config_bottom_devices_cams_right_abc");
	var $devices = $("#sett_room_config_bottom_devices_cams_right_devices");
	$devices.show();
	$abc.hide();
	otherMainContext = "left";
}


function camsSwitchFromLeftToTop(){
	var $left = $("#sett_room_config_bottom_devices_cams_left");
	var $selected = $left.find(".selected");
	$selected.removeClass("selected");
	var $devices = $("#sett_room_config_bottom_devices_cams_right_devices");
	$devices.find(".selected").removeClass("selected");
	camsEmptyLeft();
	otherContext = "top";
}

//Abc
function insertCamsAbc(){
	var $target = $("#sett_room_config_bottom_devices_cams_right_abc > ul");
	var first = true;
	for(var i =97;i<=122;i++){
		var char = String.fromCharCode(i);
		$target.append('<li char="' + char + '" ' + (first ? ' class="selected" ': '')+'>' + char + '</li>');
		first = false;
	}
	for(var i =48;i<=57;i++){
		var char = String.fromCharCode(i);
		$target.append('<li char="' + char + '" ' + (first ? ' class="selected" ': '')+'>' + char + '</li>');
		first = false;
	}
	$target.append('<li char=".">.</li>');
	$target.append('<li char="-">-</li>');
	$target.append('<li char="_">_</li>');
	$target.append('<li char=":">:</li>');
	$target.append('<li char="@">@</li>');
	$target.append('<li char="?">?</li>');
	$target.append('<li char="/">/</li>');
	$target.append('<li char=" ">space</li>');
	$target.append('<li char="shift">shift</li>');
	$target.append('<li char="backspace">back<br>space</li>');
}
function camsAbcDown(){
	nextLi($("#sett_room_config_bottom_devices_cams_right_abc").find(".selected"), 10, function(){});
}
function camsAbcUp(){
	prevLi($("#sett_room_config_bottom_devices_cams_right_abc").find(".selected"), 10, function(){});
}
function camsAbcPrev(){
	prevLi($("#sett_room_config_bottom_devices_cams_right_abc").find(".selected"), 1, function(){});
}
function camsAbcNext(){
	nextLi($("#sett_room_config_bottom_devices_cams_right_abc").find(".selected"), 1, function(){});
}
function camsAbcEnter(){
	var $abc = $("#sett_room_config_bottom_devices_cams_right_abc");
	var $selected = $abc.find(".selected");
	var $left = $("#sett_room_config_bottom_devices_cams_left");
	var $selectedLeft = $left.find(".selected");
	var $selectedLeftSpan = $selectedLeft.find("span");
	var letter = $selected.attr("char");
	if(letter == "backspace"){
		$selectedLeftSpan.html($selectedLeftSpan.text().substring(0, $selectedLeftSpan.text().length - 1));
	}else if(letter == "shift"){
		$abc.find("li").each(function(){
			var $this = $(this);
			var currLetter = $this.attr("char");
			if(currLetter.length == 1){
				var charCode = currLetter.charCodeAt(0);
				if(charCode >= 97 && charCode <= 122){
					$this.html(String.fromCharCode(charCode - 32));
					$this.attr("char", String.fromCharCode(charCode - 32));
				}else if(charCode >= 65 && charCode <= 90){
					$this.html(String.fromCharCode(charCode + 32));
					$this.attr("char", String.fromCharCode(charCode + 32));
				}
			}
		});
	}else{
		$selectedLeftSpan.html($selectedLeftSpan.html() + letter);
	}
}
//Types
function camsTypesDown(){
	nextLi($("#sett_room_config_bottom_devices_cams_right_cam_types").find(".selected"), 10, function(){});
}
function camsTypesUp(){
	prevLi($("#sett_room_config_bottom_devices_cams_right_cam_types").find(".selected"), 10, function(){});
}
function camsTypesPrev(){
	prevLi($("#sett_room_config_bottom_devices_cams_right_cam_types").find(".selected"), 1, function(){});
}
function camsTypesNext(){
	nextLi($("#sett_room_config_bottom_devices_cams_right_cam_types").find(".selected"), 1, function(){});
}
function camsTypesEnter(){
	var $types = $("#sett_room_config_bottom_devices_cams_right_cam_types");
	var $selected = $types.find(".selected");
	var $left = $("#sett_room_config_bottom_devices_cams_left");
	var $selectedLeft = $left.find(".selected");
	var $selectedLeftSpan = $selectedLeft.find("span");
	$selectedLeftSpan.text($selected.text());
}
function camsSwitchFromTypesToLeft(){
	var $types = $("#sett_room_config_bottom_devices_cams_right_cam_types");
	var $devices = $("#sett_room_config_bottom_devices_cams_right_devices");
	$devices.show();
	$types.hide();
	otherMainContext = "left";
}
function camsShowType(){
	var $types = $("#sett_room_config_bottom_devices_cams_right_cam_types");
	var $devices = $("#sett_room_config_bottom_devices_cams_right_devices");
	$devices.hide();
	$types.show();
	otherMainContext = "types";
}
function camsInsertTypes(){
	var $types = $("#sett_room_config_bottom_devices_cams_right_cam_types > ul");
	var first = true;
	$.each(camTypes, function(key, val){
		$types.append("<li type='" + val + "' class='" + (first? "selected":"") + "'>" + val + "</li>");
	});
}

function camsSaveNewCam(){
	var $name = $("#sett_room_config_bottom_devices_cams_name > span");
	var $addr = $("#sett_room_config_bottom_devices_cam_addr > span");
	var $type = $("#sett_room_config_bottom_devices_cam_type > span");
	var $login = $("#sett_room_config_bottom_devices_cams_login > span");
	var $pass = $("#sett_room_config_bottom_devices_cams_pass > span");
	var cam = new Object();
	var indx = 0;
	var $selected = null;
	if(!newCamera){
		var $devices = $("#sett_room_config_bottom_devices_cams_right_devices");
		$selected = $devices.find(".selected");
		indx = $selected.index(); 
		alert(indx);
		cam = camIds[indx];
	}
	cam.name = $name.text();
	cam.addr = $addr.text();
	cam.type = $type.text();
	cam.login = $login.text();
	cam.pass = $pass.text();
	if(newCamera){
		camIds.push(cam);
		camsAppendCamToList(cam);
		var $devices = $("#sett_room_config_bottom_devices_cams_right_devices");
		var $lis = $devices.find("li");
		$lis.removeClass("selected");
		$lis.last().addClass("selected");
	}else{
		camIds[indx] = cam;
		$selected.attr("name", cam.name);
		$selected.attr("addr", cam.addr);
		$selected.attr("type", cam.type);
		$selected.attr("login", cam.login);
		$selected.attr("pass", cam.pass);
		$selected.text(cam.name);
	}
	saveCams();
	newCamera = false;
}
function camsNewCam(){
	camsEmptyLeft();
	otherMainContext = "left";
	$("#sett_room_config_bottom_devices_cams_name").addClass("selected");
	newCamera = true;
}
function camsEmptyLeft(){
	var $name = $("#sett_room_config_bottom_devices_cams_name > span");
	var $addr = $("#sett_room_config_bottom_devices_cam_addr > span");
	var $type = $("#sett_room_config_bottom_devices_cam_type > span");
	var $login = $("#sett_room_config_bottom_devices_cams_login > span");
	var $pass = $("#sett_room_config_bottom_devices_cams_pass > span");
	$name.text("");
	$addr.text("");
	$type.text("");
	$login.text("");
	$pass.text("");
}
function camsSwitchFromLeftToDevices(){
	otherMainContext = "devices";
	var $left = $("#sett_room_config_bottom_devices_cams_left");
	$left.find(".selected").removeClass("selected");
	newCamera = false;
	showCamSelected();
}
//Cams devices nav
function camsDevDown(){
	nextLi($("#sett_room_config_bottom_devices_cams_right_devices").find(".selected"), 10, showCamSelected);
}
function camsDevUp(){
	prevLi($("#sett_room_config_bottom_devices_cams_right_devices").find(".selected"), 10, showCamSelected);
}
function camsDevPrev(){
	prevLi($("#sett_room_config_bottom_devices_cams_right_devices").find(".selected"), 1, showCamSelected);
}
function camsDevNext(){
	nextLi($("#sett_room_config_bottom_devices_cams_right_devices").find(".selected"), 1, showCamSelected);
}
function camsDevicesEnter(){
	otherMainContext = "left";
	var $name = $("#sett_room_config_bottom_devices_cams_name");
	$name.addClass("selected");
}

function camsDeleteCam(){
	var $devices = $("#sett_room_config_bottom_devices_cams_right_devices");
	var $selected = $devices.find(".selected");
	var indx = $selected.index(); 
	camIds.remove(indx);
	$selected.remove();
	saveCams();
	$devices.find("li").first().addClass("selected");
	showCamSelected();
}
