var widgetAPI = new Common.API.Widget();
var tvKey = new Common.API.TVKeyValue();
var strCamIds = getFile("cams");
alert(strCamIds);
var camIds = JSON.parse(strCamIds);
var camera_addr = "";
var img_src = "";
var move_addr = "";
var zoom_addr = "";
var retBack = false;
if(camIds && camIds.length){
	camera_addr = camIds[0].login + ":" + camIds[0].pass + "@" + camIds[0].addr;
	img_src = "http://" + camera_addr + "/jpg/image.jpg";
	move_addr = "http://" + camera_addr + "/axis-cgi/com/ptz.cgi?move=";
	zoom_addr = "http://" + camera_addr + "/axis-cgi/com/ptz.cgi?rzoom=";
}
alert(camera_addr);
var $camera_image = null;

var Main =
{

};

Main.onLoad = function()
{
	this.enableKeys();
	widgetAPI.sendReadyEvent();
	
	$("#bottombar").sfKeyHelp({
		'updown':LNG_CAMERA_UPDOWN_HELP,
		'leftright':LNG_CAMERA_LEFTRIGHT_HELP,
		'ff':LNG_CAMERA_ZOOMIN_HELP,
		'rew':LNG_CAMERA_ZOOMOUT_HELP,
		'red':LNG_CAMERA_LAST_HELP,
		'green':LNG_CAMERA_NEXT_HELP
	});
	retBack = true;
	
	document.getElementById("camera_image").setAttribute("alt", LNG_CAMERA_IMAGE_FAILED);
	
	$(document).ready(function(){
		$camera_image = $("#camera_image");
		$camera_image.attr("src", img_src);
		if(!camIds || camIds.length==0){
			$("#main").css('padding-top', '20px').css('font-size', 'x-large').css('color', 'white').text(LNG_NO_CAMERA);
		}else{
			addCameras();
		}
	});
};

Main.onUnload = function()
{
	
};

Main.enableKeys = function()
{
	document.getElementById("anchor").focus();
};


Main.keyDown = function(event)
{
	var keyCode = event.keyCode;
	alert("Key pressed: " + keyCode);

	switch(keyCode)
	{
		case tvKey.KEY_EXIT:
			$.sf.exit();
			widgetAPI.sendExitEvent();
			return true;
		break;
		case tvKey.KEY_RETURN:
		//case tvKey.KEY_PANEL_RETURN:
			if(retBack){
				window.location.href= "index.html" + window.location.search;
			}
		break;
		case tvKey.KEY_LEFT:
			camera_left();
		break;
		case tvKey.KEY_RIGHT:
			camera_right();
		break;
		case tvKey.KEY_UP:
			camera_up();
		break;
		case tvKey.KEY_DOWN:
			camera_down();
		break;
		case tvKey.KEY_FF:
		case tvKey.KEY_VOL_UP:
		case tvKey.KEY_PANEL_VOL_UP:
			camera_zoom_in();
		break;
		case tvKey.KEY_RW:
		case tvKey.KEY_VOL_DOWN:
		case tvKey.KEY_PANEL_VOL_DOWN:
			camera_zoom_out();
		break;
		case tvKey.KEY_RED:
			prevCam();
		break;
		case tvKey.KEY_GREEN:
			nextCam();
		break;
	}
	event.preventDefault();
	return false;
};

function reload_image(){
	load_image();
}
function load_image(){
	alert("reload");
	var date = new Date();
	if($camera_image){
		$camera_image.attr("src", img_src + "?x=" + date.valueOf());
	}
}
function camera_left(){
	$.get(move_addr+ "left");
}
function camera_right(){
	$.get(move_addr+ "right");
}
function camera_down(){
	$.get(move_addr+ "down");
}
function camera_up(){
	$.get(move_addr+ "up");
}
function camera_zoom_in(){
	$.get(zoom_addr+ "300");
	alert("zoom in:"+zoom_addr+ "300");
}
function camera_zoom_out(){
	$.get(zoom_addr+ "-300");
	alert("zoom out:"+zoom_addr+ "-300");
}
function addCameras(){
	var first = true;
	var $devices = $("#cam_right_sub > ul");
	$.each(camIds, function(key, val){
		var addr = val.login + ":" + val.pass + "@" + val.addr;
		$devices.append("<li addr='" + addr + "' class='" + (first ? 'selected' : '') + "'>" + val.name + "</li>");
		first = false;
	});
}
function switchToCam(addr){
	camera_addr = addr;
	img_src = "http://" + camera_addr + "/jpg/image.jpg";
	move_addr = "http://" + camera_addr + "/axis-cgi/com/ptz.cgi?move=";
	zoom_addr = "http://" + camera_addr + "/axis-cgi/com/ptz.cgi?rzoom=";
}
function nextCam(){
	var $devices = $("#cam_right_sub > ul");
	var $selected = $devices.find(".selected");
	var $next = $selected.next("li");
	if($next.length){
		switchToCam($next.attr("addr"));
		$selected.removeClass("selected");
		$next.addClass("selected");
	}
}
function prevCam(){
	var $devices = $("#cam_right_sub > ul");
	var $selected = $devices.find(".selected");
	var $next = $selected.prev("li");
	if($next.length){
		switchToCam($next.attr("addr"));
		$selected.removeClass("selected");
		$next.addClass("selected");
	}
}