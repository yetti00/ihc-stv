alert("English (en) loaded");
var LNG_ROOM = "ROOM";
var LNG_DEVICES = "DEVICES";
var LNG_FAVOURITES = "FAVOURITES";
var LNG_SCENES = "SCENES";
var LNG_LOADING_DATA = "Loading data...";
var LNG_SETTINGS = "Settings";
var LNG_CAMERAS = "Cameras";
var LNG_NO_FAVOURITES = "No favourites!";
var LNG_NAME = "Name";
var LNG_TYPE = "Type";
var LNG_PRODUCT = "Product";
var LNG_DEVICE_STATE = "Device state";
var LNG_BRIGHTNESS = "Brightness";

var LNG_DATA_FAILED = "Loading data failed!";
var LNG_NO_FAVOURITES = "No favourites!";
var LNG_NO_DEVICES = "No devices!";
var LNG_NO_ROOMS = "No rooms!";
var LNG_NO_SCENES = "No scenes!";

var LNG_CAMERA_UPDOWN_HELP = "Horizontal camera move";
var LNG_CAMERA_LEFTRIGHT_HELP = 'Vertical camera move';
var LNG_CAMERA_ZOOMIN_HELP = 'Zoom in';
var LNG_CAMERA_ZOOMOUT_HELP = 'Zoom out';
var LNG_CAMERA_LAST_HELP = 'Previous camera';
var LNG_CAMERA_NEXT_HELP = 'Next camera';
var LNG_NO_CAMERA = 'No cameras!';
var LNG_CAMERA_IMAGE_FAILED = 'Unable load image';

var LNG_OTHERS = 'OTHERS';
var LNG_CONFIGURATION = 'CONFIGURATION';

var LNG_FAVOURITES_HELP = 'Changes are saved automaticaly';
var LNG_FAVOURITES_SELECT_DEVICES = 'SELECT DEVICES TO FAVOURITES';
var LNG_FAVOURITES_SELECTED_DEVICES = 'SELECTED DEVICES TO FAVOURITES';

var LNG_SCENE_NAME = 'Scene name';
var LNG_SCENE_DEVICES = 'Scene devices list';
var LNG_SCENE_DEVICE_DESCRIPTION = 'Device description';
var LNG_SCENE_SELECT_DEVICES = 'SELECT DEVICES TO SCENE';

var LNG_SETTINGS_DEVICES = 'Devices';
var LNG_SETTINGS_ROOMS = 'Rooms';
var LNG_SETTINGS_FLOORPLAN = 'Floorplan';
var LNG_SETTINGS_PAIR = 'Pair';

var LNG_SETTINGS_ELAN = 'eLan settings';
var LNG_SETTINGS_ELAN_ADDRESS = 'eLan address:';
var LNG_SETTINGS_APP_VERSION = 'App version';

var LNG_SETTINGS_CAMERA_NAME = 'Camera name';
var LNG_SETTINGS_CAMERA_ADDRESS = 'Camera address';
var LNG_SETTINGS_CAMERA_TYPE = 'Camera type';
var LNG_SETTINGS_CAMERA_USERNAME = 'Username';
var LNG_SETTINGS_CAMERA_PASSWORD = 'Password';
var LNG_SETTINGS_CAMERA_DEVICES_TITLE = 'Complete cameras overview';

var LNG_SETTINGS_DEVICE_NAME = 'Device name';
var LNG_SETTINGS_DEVICE_ADDRESS = 'Actor address';
var LNG_SETTINGS_DEVICE_ACTOR_TYPE = 'Actor type';
var LNG_SETTINGS_DEVICE_TYPE = 'Device type';
var LNG_SETTINGS_DEVICES_TITLE = 'Complete devices overview';

var LNG_SETTINGS_ROOM_NAME = 'Room name';
var LNG_SETTINGS_PICK_FLOORPLAN = 'Pick floorplan';
var LNG_SETTINGS_FLOORPLAN_PREVIEW = 'Floorplan preview';
var LNG_SETTINGS_ROOMS_TITLE = 'Complete rooms overview';

var LNG_SETTINGS_FLOORPLANS_TITLE = 'Complete floorplans overview';

var LNG_SETTINGS_PAIR_PICK_ROOM = 'Pick room';
var LNG_SETTINGS_PAIR_DEVICE = 'Check device';
var LNG_SETTINGS_PAIR_SETUP_DEVICE_POSITION = 'Setup position on floorplan';

var LNG_SAVE = 'Save';
var LNG_DELETE = 'Delete';
var LNG_NEW_DEVICE = 'New';
var LNG_NEW_ROOM = 'New';
var LNG_SAVE_AFTER_EXIT = 'Changes are saved after exit';

var LNG_FUNCTIONS = 'Functions';

var LNG_TURN_ON_DEVICE = "Turn on device";
var LNG_TURN_OFF_DEVICE = "Turn off device";
var LNG_BLINDS_DOWN = "Blinds down";
var LNG_BLINDS_UP = "Blinds up";
var LNG_DIMM_DEVICE = "Dimm device";
var LNG_SETUP_COLOR = "Setup color";
var LNG_TURN_ON = "Turn on";
var LNG_TURN_OFF = "Turn off";
var LNG_DOWN = "Down";
var LNG_UP = "Up";