alert("Czech (cs) loaded");
var LNG_ROOM = "MÍSTNOST";
var LNG_DEVICES = "ZAŘÍZENÍ";
var LNG_FAVOURITES = "OBLÍBENÉ";
var LNG_SCENES = "SCÉNY";
var LNG_LOADING_DATA = "Načítám data...";
var LNG_SETTINGS = "Nastavení";
var LNG_CAMERAS = "Kamery";
var LNG_NO_FAVOURITES = "Žádné oblíbené!";
var LNG_NAME = "Název";
var LNG_TYPE = "Typ";
var LNG_PRODUCT = "Produkt";
var LNG_DEVICE_STATE = "Stav zařízení";
var LNG_BRIGHTNESS = "Jas";

var LNG_DATA_FAILED = "Nepodařilo se stáhnout data";
var LNG_NO_FAVOURITES = "Žádné oblíbené!";
var LNG_NO_DEVICES = "Žádné zařízení!";
var LNG_NO_ROOMS = "Žádné místnosti!";
var LNG_NO_SCENES = "Žádné scény!";

var LNG_CAMERA_UPDOWN_HELP = "Pohyb kamery horizontálně";
var LNG_CAMERA_LEFTRIGHT_HELP = 'Pohyb kamery vertikálně';
var LNG_CAMERA_ZOOMIN_HELP = 'Zoom in';
var LNG_CAMERA_ZOOMOUT_HELP = 'Zoom out';
var LNG_CAMERA_LAST_HELP = 'Předchozí kamera';
var LNG_CAMERA_NEXT_HELP = 'Další kamera';
var LNG_NO_CAMERA = 'Žádné kamery nejsou nastaveny!';
var LNG_CAMERA_IMAGE_FAILED = 'Nepodařilo se načíst obraz';

var LNG_OTHERS = 'Ostatní';
var LNG_CONFIGURATION = 'Konfigurace';

var LNG_FAVOURITES_HELP = 'Změny se ukládají automaticky';
var LNG_FAVOURITES_SELECT_DEVICES = 'VYBER ZAŘÍZENÍ DO OBLÍBENÝCH';
var LNG_FAVOURITES_SELECTED_DEVICES = 'VYBRANÁ ZAŘÍZENÍ DO OBLÍBENÝCH';

var LNG_SCENE_NAME = 'Název scény';
var LNG_SCENE_DEVICES = 'Seznam zařízení ve scéně';
var LNG_SCENE_DEVICE_DESCRIPTION = 'Popis zařízení';
var LNG_SCENE_SELECT_DEVICES = 'VYBER ZAŘÍZENÍ DO SCÉNY';

var LNG_SETTINGS_DEVICES = 'Zařízení';
var LNG_SETTINGS_ROOMS = 'Místnosti';
var LNG_SETTINGS_FLOORPLAN = 'Floorplan';
var LNG_SETTINGS_PAIR = 'Spárovat';

var LNG_SETTINGS_ELAN = 'Nastavení eLan';
var LNG_SETTINGS_ELAN_ADDRESS = 'Adresa eLanu';
var LNG_SETTINGS_APP_VERSION = 'Verze aplikace';

var LNG_SETTINGS_CAMERA_NAME = 'Název kamery';
var LNG_SETTINGS_CAMERA_ADDRESS = 'Adresa kamery';
var LNG_SETTINGS_CAMERA_TYPE = 'Typ kamery';
var LNG_SETTINGS_CAMERA_USERNAME = 'Přihlašovací jméno';
var LNG_SETTINGS_CAMERA_PASSWORD = 'Heslo';
var LNG_SETTINGS_CAMERA_DEVICES_TITLE = 'Kompletní přehled kamer';

var LNG_SETTINGS_DEVICE_NAME = 'Název zařízení';
var LNG_SETTINGS_DEVICE_ADDRESS = 'Adresa aktoru';
var LNG_SETTINGS_DEVICE_ACTOR_TYPE = 'Typ aktoru';
var LNG_SETTINGS_DEVICE_TYPE = 'Typ zařízení';
var LNG_SETTINGS_DEVICES_TITLE = 'Kompletní přehled zařízení';

var LNG_SETTINGS_ROOM_NAME = 'Název místnosti';
var LNG_SETTINGS_PICK_FLOORPLAN = 'Vyber floorplan';
var LNG_SETTINGS_FLOORPLAN_PREVIEW = 'Náhled floorplanu';
var LNG_SETTINGS_ROOMS_TITLE = 'Kompletní přehled místností';

var LNG_SETTINGS_FLOORPLANS_TITLE = 'Kompletní přehled floorplanů';

var LNG_SETTINGS_PAIR_PICK_ROOM = 'Vyber místnost';
var LNG_SETTINGS_PAIR_DEVICE = 'Přiřaď zařízení';
var LNG_SETTINGS_PAIR_SETUP_DEVICE_POSITION = 'Nastav pozici na floorplanu';

var LNG_SAVE = 'Uložit';
var LNG_DELETE = 'Vymazat';
var LNG_NEW_DEVICE = 'Nové';
var LNG_NEW_ROOM = 'Nová';
var LNG_SAVE_AFTER_EXIT = 'Změny se uloží po opuštění obrazovky';

var LNG_FUNCTIONS = 'Funkce';

var LNG_TURN_ON_DEVICE = "Sepnout zařízení";
var LNG_TURN_OFF_DEVICE = "Vypnout zařízení";
var LNG_BLINDS_DOWN = "Žaluzie dolů";
var LNG_BLINDS_UP = "Žaluzie nahoru";
var LNG_DIMM_DEVICE = "Stmívat  zařízení";
var LNG_SETUP_COLOR = "Nastavit barvu";
var LNG_TURN_ON = "Zapnout";
var LNG_TURN_OFF = "Nahoru";
var LNG_DOWN = "Dolů";
var LNG_UP = "Vypnout";
